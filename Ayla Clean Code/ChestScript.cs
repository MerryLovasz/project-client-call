﻿using UnityEngine;

public class ChestScript : Pickups {

    //reference player value script
    private PlayerValues _pVScript;
    private Gem _gem;

    // Use this for initialization
    private void Start () {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
        _gem = GetComponentInChildren<Gem>();
        _gem.Disable(); //disable the gem so that you can't pick it up before you have a key
    }

    /// <summary>
    /// When entering the trigger, if you have a key, open the chest and get the gem
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player) && _pVScript.Keys >= 1)
        {
            _pVScript.Keys -= 1;
            //TODO: play open animation here
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.chestSound;
            player.outsideSounds.Play();
            _gem.Enable();
        }
    }
}
