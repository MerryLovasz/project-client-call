﻿using UnityEngine;

public class Gem : ScorePickups {

    [SerializeField]
    [Range (10,100)] private int _gemValue; //can alter the value in the inspector

    /// <summary>
    /// when player collides with gem, add value to score
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.coinPickupSound;
            player.outsideSounds.Play();
            AddScore(_gemValue);
            Disable();
        }
    }
}
