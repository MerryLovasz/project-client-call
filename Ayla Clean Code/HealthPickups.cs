﻿using UnityEngine;

public class HealthPickups : Pickups {

    [SerializeField]
    [Range(1, 100)] private int _healthValue; //can alter the value in the inspector
    //reference player value script
    private PlayerValues _pVScript;

    // Use this for initialization
    private void Start()
    {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
    }

    /// <summary>
    /// when player collides with health and their health lowered, add health
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player) && _pVScript.Health < _pVScript.maxHealth)
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.healthPickupSound;
            player.outsideSounds.Play();
            _pVScript.Damage(-_healthValue);
            Disable();
        }
    }
}
