﻿using UnityEngine;

public class KeyScript : Pickups {
    
    //reference player value script
    private PlayerValues _pVScript;

    // Use this for initialization
    private void Start ()
    {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();

    }
	
	/// <summary>
    /// when player collides with a key, add the key to inventory
    /// </summary>
    /// <param name="other"></param>
	private void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.keySound;
            player.outsideSounds.Play();
            _pVScript.Keys += 1;
            Disable();
        }
	}
}
