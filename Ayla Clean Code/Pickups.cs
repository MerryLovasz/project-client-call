﻿using UnityEngine;

public class Pickups : MonoBehaviour {

    //can be used to check whether sth is picked up
    private bool _isPickedup;

    //enable object to pickup
    public void Enable()
    {
        _isPickedup = false;
        this.gameObject.SetActive(true);
    }

    //disable when object is pickedup
    public void Disable()
    {
        _isPickedup = true;
        this.gameObject.SetActive(false);
    }
}
