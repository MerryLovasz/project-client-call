﻿using UnityEngine;
using UnityEngine.UI;

public sealed class PlayerManager : MonoBehaviour
{
    //The instance always begins with being null
    private static PlayerManager _instance = null;
    //Values the manager is keeping
    private int _scorePlayer;
    private int _livesPlayer;
    private bool _foundDeath = false;
    //references to objects
    private GameObject _deathScreen;
    private Text Score;

    /// <summary>
    /// Create the manager
    /// </summary>
    public static PlayerManager instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject playerManager = new GameObject();
                playerManager.name = "Manager";
                _instance = playerManager.AddComponent<PlayerManager>();
                DontDestroyOnLoad(playerManager);
            }
            return _instance;
        }
    }

    /// <summary>
    /// When the manager gets enabled
    /// </summary>
    private void OnEnable()
    {
        ScorePlayer = 0;
        LivesPlayer = 3;
    }

    /// <summary>
    /// Searches for objects so that they aren't null, and sets deathscreen to false or true
    /// </summary>
    private void Update()
    {
        //make sure those objects aren't null
        if (Score == null)
        {
            Score = GameObject.Find("ScoreText2").GetComponent<Text>();
        }
        if (_deathScreen == null)
        {
            _deathScreen = GameObject.Find("DeathScreen");
        }
        //beginning of scene, set deathscreen to false 
        if (_deathScreen != null && _deathScreen.activeInHierarchy && !FoundDeath)
        {
            _deathScreen.SetActive(false);
        }
        //when you die, set active
        if (_deathScreen != null && !_deathScreen.activeInHierarchy && FoundDeath)
        {
            _deathScreen.SetActive(true);
        }
    }

    /// <summary>
    /// what happens when player loses a life or dies
    /// </summary>
    public void LoseLife()
    {
        LivesPlayer -= 1;
        if (LivesPlayer <= 0)
        {
            FoundDeath = true;
            _deathScreen.SetActive(true);
            Time.timeScale = 0.0f;
            Score.text = "Total Score: " + ScorePlayer;
        }
    }

    /// <summary>
    /// Getters and setters for the values that PlayerValuesScript needs to access
    /// </summary>
    public int ScorePlayer
    {
        get { return _scorePlayer; }
        set
        {
            _scorePlayer = value;
        }
    }
    public int LivesPlayer
    {
        get { return _livesPlayer; }
        set
        {
            _livesPlayer = value;
        }
    }
    public bool FoundDeath
    {
        get { return _foundDeath; }
        set
        {
            _foundDeath = value;
        }
    }
}