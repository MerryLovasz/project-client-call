﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneloaderScript : MonoBehaviour
{
    [SerializeField]
    private int _sceneNumber;

    /// <summary>
    /// Function for loading a scene with the index number
    /// </summary>
    /// <param name="sceneBuildIndex"></param>
    public void LoadScene(int sceneBuildIndex)
    {
        SceneManager.LoadScene(sceneBuildIndex);
    }

    /// <summary>
    /// Function for loading a scene with the scene name
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// When player enters the trigger, load a new scene
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            LoadScene(_sceneNumber);
        }
    }
}
