﻿using UnityEngine;

public class ScorePickups : Pickups {

    //reference to player value script
    protected PlayerValues _pVScript;

    // Use this for initialization
    private void Start()
    {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
    }

    /// <summary>
    /// Adds the value of the picked up object to the score
    /// </summary>
    /// <param name="_score"></param>
	protected void AddScore(int _score)
    {
        _pVScript.Score += _score;
    }
}
