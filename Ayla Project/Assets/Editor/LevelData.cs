﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


public class LevelData  {
    [XmlArray("Objects")]
    [XmlArrayItem("Object")]
    public List<EditorObject> objects = new List<EditorObject>();
    //[XmlAttribute("name")]
    //public string Name;
    //public List<Vector3> platforms = new List<Vector3>();
    //public List<Vector3> coins1 = new List<Vector3>();
    //public List<Vector3> gems1 = new List<Vector3>();
    //public List<Vector3> gems2 = new List<Vector3>();
    //public List<Vector3> gems3 = new List<Vector3>();
    //public List<Vector3> cannonball = new List<Vector3>();
    //public List<Vector3> explosiveball = new List<Vector3>();
    //public List<Vector3> enemy = new List<Vector3>();
    //public List<Vector3> waypoint = new List<Vector3>();


}
