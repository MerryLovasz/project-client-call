﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
[ExecuteInEditMode]
public class LevelEditor : MonoBehaviour
{
    XmlSerializer _xmlSer = new XmlSerializer();
    LevelData _writeData = new LevelData();
    LevelData _readData = new LevelData();

    [SerializeField]
    private string _fileName = "level 1";

    public void Read()
    {
        _readData = _xmlSer.ReadXML(_fileName);
        GameObject parent = new GameObject("" + _fileName);
        foreach (EditorObject item in _readData.objects)
        {
            GameObject obj = Resources.Load("Prefabs/" + item.Name) as GameObject;
            if (obj != null)
            {
                GameObject GO = GameObject.Instantiate(obj);
                GO.transform.position = item.Pos;
                GO.transform.localScale = item.Scale;
                GO.transform.rotation = item.Rotation;
                GO.transform.SetParent(parent.transform);
            }
        }
    }
    public void Save()
    {
        GameObject levelParent = GameObject.Find("Level");
        foreach (Transform child in levelParent.transform)
        {
            foreach (Transform prefab in child)
            {
                GameObject prefabParent = UnityEditor.PrefabUtility.GetPrefabParent(prefab.gameObject) as GameObject;
                if (prefabParent != null)
                {
                    EditorObject editObj = new EditorObject();
                    editObj.Name = prefabParent.name;
                    editObj.Pos = prefab.position;
                    editObj.Scale = prefab.localScale;
                    editObj.Rotation = prefab.rotation;
                    _writeData.objects.Add(editObj);
                }
                else
                {
                    Debug.Log("No prefab");
                }

            }
        }
        _xmlSer.WriteXML(_writeData, _fileName);
    }
}
