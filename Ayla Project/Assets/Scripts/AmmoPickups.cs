﻿using UnityEngine;
using System.Collections;

public class AmmoPickups : Pickups {

    //reference to player values
    protected PlayerValues _pVScript;

    private void Start()
    {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
    }

    protected void AddAmmo(int ammoValue)
    {
        _pVScript.Cannonballs += ammoValue;
    }
}
