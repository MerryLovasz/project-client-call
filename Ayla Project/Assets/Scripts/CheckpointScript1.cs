﻿using UnityEngine;
using System.Collections;

public class CheckpointScript1 : MonoBehaviour {

    //reference to PlayerValues
    private PlayerValues _pVScript;

	// Use this for initialization
	void Start () {
        _pVScript = FindObjectOfType<PlayerValues>(); //search for the player and then get PlayerValues
    }
	
    //when the player enters a checkpoint, plus one the array index and deactivate the object
	void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            _pVScript._checkpoint += 1;
            this.gameObject.SetActive(false);
        }
    }
}
