﻿using UnityEngine;
using System.Collections;

public class ChestScript : Pickups {

    //reference player value script
    private PlayerValues _pVScript;

    private Gem _gem;

    void Start () {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
        _gem = GetComponentInChildren<Gem>();
        _gem.Disable();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player) && _pVScript.Keys >= 1)
        {
            _pVScript.Keys -= 1;
            //TODO: play open animation here
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.chestSound;
            player.outsideSounds.Play();
            _gem.Enable();
        }
    }
}
