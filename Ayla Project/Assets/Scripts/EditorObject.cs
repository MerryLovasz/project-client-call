﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

public class EditorObject {
   
    [XmlAttribute("Type")]
    public string Name;
    public Vector3 Pos;
    public Vector3 Scale;
    public Quaternion Rotation;

    public EditorObject()
    {

    }
}
