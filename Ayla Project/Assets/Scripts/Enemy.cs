﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private int _health;
    [SerializeField]
    private int _meleeDamage;
    [SerializeField]
    private int _rangedDamage;
    [SerializeField]
    private float _walkSpeed;
    [SerializeField]
    private float _bulletSpeed;
    [SerializeField]
    private float _attackCooldown;
    [SerializeField]
    private float _attackTimer;
    [SerializeField]
    private float _meleeDistance;
    [SerializeField]
    private float _rangedDistance;
    //[SerializeField]
    //private float followDistance;
    [SerializeField]
    private float _playerDelta;
    [SerializeField]
    private bool _canAttack;
    [SerializeField]
    private float _score;
    private GameObject _player;
    [SerializeField]
    private List<Transform> _waypoints = new List<Transform>();
    [SerializeField]
    private GameObject _bullet;
    [SerializeField]
    private bool _isMeleeing = false;
    
    private NavMeshAgent _navAgent;

    [SerializeField]
    private AttackType _combatType;
    [SerializeField]
    private State _currentState;
    private State _beginState;
    private PlayerControls _pControl;
    private GameObject _currentPlatform;
    private int _currentWaypoint;
    private float _yRot;

    private enum AttackType
    {
        Melee, Ranged, Both
    }
    private enum State
    {
        Idle, Patrol, Attack, Dead
    }

    void Start()
    {
        _navAgent = GetComponent<NavMeshAgent>();
        _navAgent.speed = _walkSpeed;
        _navAgent.updateRotation = false;
        _currentWaypoint = 0;
        SwitchState(_currentState);
        _beginState = _currentState;
        _player = FindObjectOfType<PlayerControls>().gameObject;
        _pControl = _player.GetComponent<PlayerControls>();
    }

    void Update()
    {
        //distance between player and the enemy
        _playerDelta = Vector3.Distance(transform.position, _player.transform.position);
        checkState();
    }
    private void checkState()
    {
        if (_currentState == State.Patrol)
        {
            _yRot = (_navAgent.velocity.z > 0.0f) ? 0.0f : 180.0f;
            setRot();
            //Waypoints
            if (_navAgent.remainingDistance < 0.25f)
            {
                _currentWaypoint++;
                if (_currentWaypoint > _waypoints.Count - 1) _currentWaypoint = 0;
                _navAgent.destination = _waypoints[_currentWaypoint].position;
            }
            //check for the combat type
            switch (_combatType)
            {
                case AttackType.Melee:
                    if (doRayCast() == true && _playerDelta <= _meleeDistance)
                    {
                        SwitchState(State.Attack);
                    }
                    break;
                case AttackType.Ranged:
                    if (doRayCast() == true && _playerDelta <= _rangedDistance)
                    {
                        SwitchState(State.Attack);
                    }
                    break;
                case AttackType.Both:
                    if (doRayCast() == true && _playerDelta <= _rangedDistance)
                    {
                        SwitchState(State.Attack);
                    }
                    break;
                default:
                    break;
            }


            if (doRayCast() == true && _playerDelta <= _meleeDistance)
            {
                SwitchState(State.Attack);
            }


        }
        else if (_currentState == State.Idle)
        {
            //can I see the player and is the player in the range distance?
            if (doRayCast() == true && _playerDelta <= _rangedDistance)
            {
                switch (_combatType)
                {
                    case AttackType.Melee:
                        if (_playerDelta <= _meleeDistance)
                        {
                            SwitchState(State.Attack);
                        }
                        break;
                    case AttackType.Ranged:
                        SwitchState(State.Attack);
                        break;
                    case AttackType.Both:
                        SwitchState(State.Attack);
                        break;
                    default:
                        break;
                }
            }
        }
        if (_currentState == State.Attack)
        {
            _yRot = ((_player.transform.position.z - transform.position.z) > 0.0f) ? 0.0f : 180.0f;

            setRot();
            if (_canAttack == true)
            {
                attack();
                _canAttack = false;
            }
            else if (_canAttack == false)
            {
                _attackTimer += Time.deltaTime;

                if (_attackTimer >= _attackCooldown)
                {
                    _attackTimer = 0;
                    _canAttack = true;
                }
            }
        }
    }
    private bool doRayCast()
    {
        RaycastHit hit;
        Vector3 rayDirection = (_player.transform.position - transform.position).normalized;
        if (Physics.Raycast(transform.position, rayDirection, out hit))
        {
            if (hit.collider.gameObject.CompareTag(Tags.Player))
            {
                return true;
            }
        }
        else
        {
            Debug.Log(gameObject.name + " cannot see player.");
            return false;
        }
        return false;
    }
    private void SwitchState(State pState)
    {
        switch (pState)
        {
            case State.Idle:
                //play idle animation
                Debug.Log(gameObject.name + " Changed to idle state");
                _navAgent.Stop();
                break;
            case State.Patrol:
                //play walking animation
                Debug.Log(gameObject.name + " Changed to patrol state");
                _navAgent.Resume();
                break;
            case State.Attack:
                Debug.Log(gameObject.name + " Changed to attack state");
                _navAgent.Stop();
                break;
            case State.Dead:
                Debug.Log(gameObject.name + " Changed to dead state");
                _navAgent.Stop();
                //animation
                Destroy(this.gameObject);
                break;
        }
        _currentState = pState;
    }

    private void attack()
    {
        switch (_combatType)
        {
            case AttackType.Melee:
                if (_playerDelta <= _meleeDistance)
                {
                    melee();
                }
                else
                {
                    SwitchState(_beginState);
                }
                break;
            case AttackType.Ranged:
                if (_playerDelta <= _rangedDistance)
                {
                    shoot();
                }
                else
                {
                    SwitchState(_beginState);
                }
                //check for ranged distance
                //if player is within the distance --> shoot()
                break;
            case AttackType.Both:
                //if player distance is inbetween shooting distance and melee distance --> shoot()
                if (_playerDelta <= _rangedDistance && _playerDelta > _meleeDistance)
                {
                    shoot();
                }
                //if player distance if in shooting AND melee distance --> melee
                else if (_playerDelta <= _rangedDistance && _playerDelta <= _meleeDistance)
                {
                    melee();
                }
                else if (_playerDelta > _rangedDistance)
                {
                    SwitchState(State.Patrol);

                }
                break;
        }
    }

    private void melee()
    {
        PlayerValues pvScript = _player.GetComponent<PlayerValues>();
        pvScript.Damage(_meleeDamage);
        pvScript.IgnoreColl(true,GetComponent<CapsuleCollider>());

        //float knockbackValue = 2;
        //float direction = (_yRot == 180.0f) ? -1 : 1;
        //Rigidbody _rigid = _pControl.GetComponent<Rigidbody>();
        //_rigid.AddForce((new Vector3(0, 1, direction) * 5 * Time.deltaTime) * 100, ForceMode.Impulse);
        ////_rigid.AddForce((new Vector3(0, 0, direction) * 100 * Time.deltaTime) * 100, ForceMode.Impulse);
    }

    private void shoot()
    {
        Debug.Log("Bang!");
        Quaternion rot = transform.rotation;
        Vector3 pos = transform.position;
        GameObject projectile = Instantiate(_bullet);
        ProjectileScript projScript = projectile.GetComponent<ProjectileScript>();
        projScript.projOwner = ProjectileScript.Owner.Enemy;
        projScript.Damage = _rangedDamage;
        projectile.transform.position = pos + transform.transform.forward;
        projectile.GetComponent<Rigidbody>().AddForce((transform.transform.forward * _bulletSpeed * Time.deltaTime) * 100, ForceMode.Impulse);
    }

    private void setRot()
    {
        Quaternion rot = transform.rotation;
        rot = new Quaternion(rot.x, _yRot, rot.z, rot.w);
        transform.rotation = rot;
    }
    public void Damage(int damage)
    {
        if (damage >= _health)
        {
            SwitchState(State.Dead);
        }
        else
        {
            _health -= damage;
        }
    }

    public int Health
    {
        get { return _health; }
        set { _health = value; }
    }

    public int MeleeDamage
    {
        get { return _meleeDamage; }
        set { _meleeDamage = value; }
    }
    public int RangedDamage
    {
        get { return _rangedDamage; }
        set { _rangedDamage = value; }
    }
    public float WalkSpeed
    {
        get { return _walkSpeed; }
        set { _walkSpeed = value; }
    }

    public float BulletSpeed
    {
        get { return _bulletSpeed; }
        set { _bulletSpeed = value; }
    }

    public float MeleeDistance
    {
        get { return _meleeDistance; }
        set { _meleeDistance = value; }
    }
    public float RangedDistance
    {
        get { return _rangedDistance; }
        set { _rangedDistance = value; }
    }
}
