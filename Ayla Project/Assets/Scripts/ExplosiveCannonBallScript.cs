﻿using UnityEngine;
using System.Collections;

public class ExplosiveCannonBallScript : AmmoPickups
{
    //when player collides with the ball, add to inventory
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.pickupCannonSound;
            player.outsideSounds.Play();
            _pVScript.ExplosiveCannonballs += 1;
            Disable();
        }
    }
}
