﻿using UnityEngine;
using System.Collections;

public class FallDeathScript : MonoBehaviour {

    //Reference
    private PlayerManager pManager;
    [SerializeField]
    private GameObject _spawnArea;
    [SerializeField]
    private GameObject _player;

    private void Start()
    {
        pManager = FindObjectOfType<PlayerManager>();
    }

    // Update is called once per frame
    void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            pManager.LoseLife();
            _player.transform.position = _spawnArea.transform.position;
        }
	}
}
