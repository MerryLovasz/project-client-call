﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{

    [SerializeField]
    private SceneloaderScript _sceneLoader;
    private PlayerManager pManager;

    private string[] _menuButtons = new string[2] {
        "Play",
        "Exit"
    };

    private int _selectedIndex = 1;
    private bool m_isAxisInUse;

    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneloaderScript>();
        pManager = PlayerManager.instance;
        pManager.gameObject.SetActive(false);
    }

    private int MenuSelection(string[] menuItems, int selectedItem, string direction)
    {
        if (direction == "up")
        {
            if (selectedItem == 0)
            {
                selectedItem = menuItems.Length - 1;
            }
            else
            {
                selectedItem -= 1;
            }
        }

        if (direction == "down")
        {
            if (selectedItem == menuItems.Length - 1)
            {
                selectedItem = 0;
            }
            else
            {
                selectedItem += 1;
            }
        }
        return selectedItem;
    }

    void Update()
    {
        if (Input.GetAxisRaw("Vertical") != 0)
        {
            if (m_isAxisInUse == false)
            {
                // Call your event function here.
                _selectedIndex = MenuSelection(_menuButtons, _selectedIndex, "down");
                GetComponent<AudioSource>().Play();
                m_isAxisInUse = true;
            }
        }
        if (Input.GetAxisRaw("Vertical") == 0)
        {
            m_isAxisInUse = false;
        }
    }

    private void OnGUI()
    {
        GUI.SetNextControlName("Play");
        if (GUI.Button(new Rect(Screen.width / 2.3f, Screen.height / 2, Screen.height / 3, Screen.width / 40), "Play"))
        {
            GetComponent<AudioSource>().Play();
            pManager.gameObject.SetActive(true);
            _sceneLoader.LoadScene(1); //start the game
        }
        GUI.SetNextControlName("Exit");
        if (GUI.Button(new Rect(Screen.width / 2.3f, Screen.height / 1.5f, Screen.height / 3, Screen.width / 40), "Exit"))
        {
            GetComponent<AudioSource>().Play();

            Application.Quit(); //quit the game
        }

        GUI.FocusControl(_menuButtons[_selectedIndex]);
    }
}