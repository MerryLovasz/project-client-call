﻿using UnityEngine;
using System.Collections;

public class Pickups : MonoBehaviour {

    //can be used to check whether sth ir picked up
    private bool _isPickedup;

    //enable object to pickup
    public void Enable()
    {
        _isPickedup = false;
        this.gameObject.SetActive(true);
    }

    //disbale when object is pickedup
    public void Disable()
    {
        _isPickedup = true;
        this.gameObject.SetActive(false);
    }
}
