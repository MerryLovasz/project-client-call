﻿using UnityEngine;
using UnityEngine.UI;

public sealed class PlayerManager : MonoBehaviour
{
    private static PlayerManager _instance = null;
    public int ScorePlayer;
    public int LivesPlayer;
    public bool FoundDeath = false;
    //references to objects
    private GameObject _deathScreen;
    private Text Score;

    //Create the manager
    public static PlayerManager instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject playerManager = new GameObject();
                playerManager.name = "Manager";
                _instance = playerManager.AddComponent<PlayerManager>();
                DontDestroyOnLoad(playerManager);
            }
            return _instance;
        }
    }



    // Use this for initialization
    void OnEnable()
    {
        ScorePlayer = 0;
        LivesPlayer = 3;
    }

    void Update()
    {
        //make sure those objects aren't null
        if (Score == null)
        {
            Score = GameObject.Find("ScoreText2").GetComponent<Text>();
        }
        if (_deathScreen == null)
        {
            _deathScreen = GameObject.Find("DeathScreen");
        }
        //beginning of scene, set deathscreen to false 
        if (_deathScreen != null && _deathScreen.activeInHierarchy && !FoundDeath)
        {
            _deathScreen.SetActive(false);
        }
        //when you die, set active
        if (_deathScreen != null && !_deathScreen.activeInHierarchy && FoundDeath)
        {
            _deathScreen.SetActive(true);
        }
    }

    //what happens when player loses a life or dies
    public void LoseLife()
    {
        LivesPlayer -= 1;

        if (LivesPlayer <= 0)
        {
            FoundDeath = true;
            _deathScreen.SetActive(true);
            Time.timeScale = 0.0f;
            Score.text = "Total Score: " + ScorePlayer;
        }
    }
}