﻿using UnityEngine;
using System.Collections;

public static class Tags {
    public static string Player = "Player";
    public static string Enemy = "Enemy";
    public static string Sword = "Sword";
    public static string Cannon = "Cannon";
    public static string Pickup = "Pickup";
    public static string Projectile = "Projectile";
    public static string Movable = "Movable";
    public static string TeleportObject = "TeleportObject";
    public static string Destroyable = "Destroyable";
    public static string SwordPickup = "SwordPickup";
    public static string CannonPickup = "CannonPickup";

}
