﻿using UnityEngine;

public class WinScreenButton : MonoBehaviour
{

    [SerializeField]
    private SceneloaderScript _sceneLoader;

    private string[] _menuButtons = new string[1] {
        "Back"
    };

    private int _selectedIndex = 0;

    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneloaderScript>();
    }

    //create the button to go back to menu
    private void OnGUI()
    {
        GUI.SetNextControlName("Back");
        if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 1.5f, Screen.height / 3, Screen.width / 40), "Back to Menu"))
        {
            Time.timeScale = 1.0f;
            _sceneLoader.LoadScene(0); //back to menu
        }

        GUI.FocusControl(_menuButtons[_selectedIndex]);
    }
}

