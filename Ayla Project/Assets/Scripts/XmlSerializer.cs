﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

public class XmlSerializer  {

    public void WriteXML(LevelData lvl, string pName)
    {
        System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(LevelData));
        var path = "Assets/Levels/" + pName + ".xml";
        FileStream stream = File.Create(path);

        writer.Serialize(stream, lvl);
        stream.Close();
    }

    public LevelData ReadXML(string pName)
    {
        System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(LevelData));
        var path = "Assets/Levels/" + pName + ".xml";
        FileStream stream = File.OpenRead(path);

        LevelData readData = writer.Deserialize(stream) as LevelData;
        stream.Close();
        return readData;
    }
}
