﻿using UnityEngine;
using System.Collections;

public class DeathScreenButton : MonoBehaviour {

    [SerializeField]
    private SceneloaderScript _sceneLoader;

    private PlayerManager pManager;

    private string[] _menuButtons = new string[1] {
        "Back"
    };

    private int _selectedIndex = 0;

    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneloaderScript>();
    }

    void Update()
    {
        if(pManager == null)
        {
            pManager = GameObject.Find("Manager").GetComponent<PlayerManager>();
        }
    }
    private void OnGUI()
    {
        GUI.SetNextControlName("Back");
        if (GUI.Button(new Rect(Screen.width / 2.3f, Screen.height / 2, Screen.height / 3, Screen.width / 40), "Back to Menu"))
        {
            Time.timeScale = 1.0f;
            _sceneLoader.LoadScene(0); //back to menu
            pManager.ScorePlayer = 0;
            pManager.LivesPlayer = 3;
            pManager.FoundDeath = false;
        }

        GUI.FocusControl(_menuButtons[_selectedIndex]);
    }
}

