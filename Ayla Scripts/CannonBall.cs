﻿using UnityEngine;
using System.Collections;

public class CannonBall : AmmoPickups {

    private int _cannonValue = 1;

    //when player collides with cannonball, add ball to inventory
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Disable();
            //play sound/ anim(implement)
            AddAmmo(_cannonValue);
        }
    }
}
