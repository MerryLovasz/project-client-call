﻿using UnityEngine;
using System.Collections;

public class ChestScript : Pickups {

    //reference player value script
    private PlayerValues _pVScript;

    private Gem _gem;

    // Use this for initialization
    void Start () {
        _pVScript = GameObject.FindWithTag("Player").GetComponent<PlayerValues>();
        _gem = GetComponentInChildren<Gem>();
        _gem.Disable();
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && _pVScript.Keys >= 1)
        {
            _pVScript.Keys -= 1;
            //play open animation & sound here
            _gem.Enable();
        }
    }
}
