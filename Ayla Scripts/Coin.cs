﻿using UnityEngine;
using System.Collections;

public class Coin : ScorePickups {

    [SerializeField]
    [Range (1,10)] private int _coinValue; //can alter the value in the inspector

    //when player collides with coin, add it to the score
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AddScore(_coinValue);
            //play sound/ anim(implement)
            Disable();
        }
    }
}
