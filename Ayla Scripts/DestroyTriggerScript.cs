﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DestroyTriggerScript : MonoBehaviour {

    [SerializeField]
    private GameObject _object; //the object you want to destroy. Can make it into an array if you want to destroy multiple objects at once.
    [SerializeField]
    private Text _help; //the text with the info for the player on how to shoot
    private  PlayerValues _pVScript;

    // Use this for initialization
    private void Start () {
        _pVScript = GameObject.FindWithTag("Player").GetComponent<PlayerValues>();
        _help.gameObject.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && _pVScript.ExplosiveCannonballs) 
        {
            _help.gameObject.SetActive(true);
            DestroyObject();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _help.gameObject.SetActive(false); //disable text when leaving trigger
    }

    private void DestroyObject()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //play animation and sound (implement)
            //destroy object plus trigger and text.
            _object.SetActive(false); 
            _help.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }
}
