﻿using UnityEngine;
using System.Collections;

public class ExplosiveCannonBallScript : AmmoPickups
{
    //when player collides with the ball, add to inventory
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_pVScript.ExplosiveCannonballs)
        {
            _pVScript.ExplosiveCannonballs = true;
            //play sound/ anim(implement)
            Disable();
        }
    }
}
