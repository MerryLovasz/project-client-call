﻿using UnityEngine;
using System.Collections;

public class Gem : ScorePickups {

    [SerializeField]
    [Range (10,100)] private int _gemValue;

    //when player collides with gem, add value to score
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AddScore(_gemValue);
            //play sound/anim (implement)
            Disable();
        }
    }
}
