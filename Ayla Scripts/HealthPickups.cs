﻿using UnityEngine;
using System.Collections;

public class HealthPickups : Pickups {

    //can change how much health is added
    [SerializeField]
    [Range(1, 10)]
    private int _healthValue;

    //reference player value script
    private PlayerValues _pVScript;

    void Start()
    {
        _pVScript = GameObject.FindWithTag("Player").GetComponent<PlayerValues>();
    }

    //when player collides with health and their health lowered, add health
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && _pVScript.PlayerHealth < _pVScript.maxHealth)
        {
            //play sound/ anim(implement)
            //add health
            _pVScript.PlayerHealth += _healthValue;
            //remove pickup
            Disable();
        }
    }
}
