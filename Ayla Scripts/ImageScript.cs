﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageScript : MonoBehaviour {

    [SerializeField]
    private Image _info; //the image with the info/dialogue for the player on how to shoot

    // Use this for initialization
    void Start () {
        _info.gameObject.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _info.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _info.gameObject.SetActive(false); //disable image when leaving trigger
    }
}
