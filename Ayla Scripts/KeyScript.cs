﻿using UnityEngine;
using System.Collections;

public class KeyScript : Pickups {

    //reference player value script
    private PlayerValues _pVScript;

    // Use this for initialization
    void Start ()
    {
        _pVScript = GameObject.FindWithTag("Player").GetComponent<PlayerValues>();
    }
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _pVScript.Keys += 1;
            Disable();
        }
	}
}
