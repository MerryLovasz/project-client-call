﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

    [SerializeField]
    private Button _exit;

    //quit the game
    public void Quit()
    {
        Application.Quit();
    }
}
