﻿using System;
using UnityEngine;
using UnityEngine.UI;

public sealed class PlayerManager : MonoBehaviour {

    public int ScorePlayer;
    public int LivesPlayer;

    [SerializeField]
    private GameObject _deathScreen;
    [SerializeField]
    private Text Score;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

	// Use this for initialization
	void Start ()
    {
        _deathScreen.SetActive(false);
        LivesPlayer = 3;

        //if (FindObjectOfType<PlayerManager>() != null)
        //{
        //    Destroy(gameObject);
        //}
    }

    public void LoseLife()
    {
        LivesPlayer -= 1;

        if (LivesPlayer <= 0)
        {
            _deathScreen.SetActive(true);
            Score.text = "Total Score: " + ScorePlayer;
        }
    }
}
