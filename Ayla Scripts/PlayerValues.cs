﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerValues : MonoBehaviour {

    //UI
    public Slider healthSlider;
    public Text TotalScore;
    public Text CannonCount;
    public Text ExplosiveCount;

    //Values
    [Range(0f, 100f)]
    public int Health;
    [HideInInspector]
    public int maxHealth;
    [HideInInspector]
    public int Cannonballs = 0;
    [HideInInspector]
    public bool ExplosiveCannonballs = false;
    [HideInInspector]
    public int Score = 0;
    [HideInInspector]
    public int Keys = 0;

    [SerializeField]
    private GameObject _spawnArea;
    [SerializeField]
    private GameObject _player;

    //Reference
    private PlayerManager pManager;

    // Use this for initialization
    private void Start () {
        pManager = FindObjectOfType<PlayerManager>();
        if (pManager.ScorePlayer != 0)
        {
            Score = pManager.ScorePlayer;
        }
        maxHealth = Health; //set maxhealth to health when the level starts
        healthSlider.maxValue = maxHealth; //set slider to correct max value
    }
    
    public void Damage(int pDamage)
    {
        if (pManager.LivesPlayer > 0)
        {
            if (pDamage >= Health)
            {
                Debug.Log("Player got hit by a fatal hit!");
                pManager.LoseLife();
                _player.transform.position = _spawnArea.transform.position;
                Health = maxHealth;
            }
            else if (pDamage < Health)
            {
                Health -= pDamage;
                Debug.Log("Player got damaged for " + pDamage);
            }
        }
        else
        {
            //show death scene and go back to menu
            Debug.Log("Player should be dead; lives not handled yet.");
        }
    }

    private void Update()
    {
        if (Score != 0)
            pManager.ScorePlayer = Score;
        //update slider
        healthSlider.value = Health; 
        //updte UI
        TotalScore.text = "Score: " + Score;
        CannonCount.text = "Cannonballs: " + Cannonballs;
        if(ExplosiveCannonballs)
        ExplosiveCount.text = "Explosive Cannonballs: 1";
        if(!ExplosiveCannonballs)
        ExplosiveCount.text = "Explosive Cannonballs: 0";
    }
}
