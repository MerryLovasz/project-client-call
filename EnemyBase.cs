﻿using UnityEngine;
using System.Collections;

public class EnemyBase : MonoBehaviour {

    //creates the standard stats for an enemy
    private int speed;
    [SerializeField]
    private int defaultSpeed;
    private bool died;
    [SerializeField]
    private int chaseSpeed;
    [SerializeField]
    private int targetRange;
    [SerializeField]
    private int damage;
    [SerializeField]
    private int targetAttackRange;
    [SerializeField]
    private int attackCooldown;
    [SerializeField]
    private int baseHealth;
    private int health;
    [SerializeField]
    private int fov = 80;
    [SerializeField]
    private int respawnTimer;
    private int respawnTimeLeft;
    [SerializeField]
    private GameObject deathEffect;
    private GameObject targetObject;
    private Transform target;

    //Getters and setters for all the stats
    public GameObject DeathEffect
    {
        get { return deathEffect; }
    }

    public int Fov
    {
        get { return fov; }
    }

    public int RespawnTimer
    {
        get { return respawnTimer; }
    }

    public int RespawnTimeLeft
    {
        get { return respawnTimeLeft; }
        set { respawnTimeLeft = value; }
    }

    public int Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    public int ChaseSpeed
    {
        get { return chaseSpeed; }
     }

    public int DefaultSpeed
    {
        get { return defaultSpeed; }
    }

    public int Damage
    {
        get { return damage; }
    }

    public int BaseHealth
    {
        get { return baseHealth; }
        set { baseHealth = value; }
    }

    public int TargetRange
    {
        get { return targetRange; }
    }

    public int TargetAttackRange
    {
        get { return targetAttackRange; }
    }

    public int AttackCooldown
    {
        get { return attackCooldown; }
        set { attackCooldown = value; }
    }

    public Transform Target
    {
        get { return target; }
        set { target = value; }
    }

    public GameObject TargetObject
    {
        get { return targetObject; }
        set { targetObject = value; }
    }

    public bool Died
    {
        get { return died; }
        set { died = value; }
    }

    public int Health
    {
        get { return health; }
        set { health = value; }
    }
}
