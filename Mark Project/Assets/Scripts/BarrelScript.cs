﻿using UnityEngine;
using System.Collections;

public class BarrelScript : MonoBehaviour
{

    [SerializeField]
    private GameObject _healthPickup;
    [SerializeField]
    private GameObject _pickupLocation;
    [SerializeField]
    private bool _dropsPickup = true;

    // Use this for initialization
    void Start()
    {
        _healthPickup.SetActive(false);
    }

    // Update is called once per frame

    public void DoDestroy()
    {
        if (_dropsPickup == true)
        {
            _healthPickup.SetActive(true);
            GameObject healthPickup = Instantiate(_healthPickup);
            healthPickup.transform.position = _pickupLocation.transform.position;
        }
        Destroy(this.gameObject);
    }

    public void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag(Tags.Projectile))
        {
            ProjectileScript projectile = c.GetComponent<ProjectileScript>();
            if (projectile.projOwner == ProjectileScript.Owner.Player)
            {
                DoDestroy();
            }
        }
    }

}
