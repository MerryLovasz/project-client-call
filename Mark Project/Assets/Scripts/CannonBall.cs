﻿using UnityEngine;
using System.Collections;

public class CannonBall : AmmoPickups {

    private int _cannonValue = 1;

    //when player collides with cannonball, add ball to inventory
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.pickupCannonSound;
            player.outsideSounds.Play();
            Disable();
            AddAmmo(_cannonValue);
        }
    }
}
