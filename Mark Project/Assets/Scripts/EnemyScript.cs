﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
    [SerializeField] [Range(0f, 200f)]
    private int _health = 100;
    private Animator _anim;

    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    public int GetDamage(int pDamage)
    {
        _health -= pDamage;
        return _health;
    }

    private void Update()
    {
        UpdateDeath();
    }

    private void UpdateDeath()
    {
        if (_health <= 0) {
            Destroy(gameObject);

            //Still need an animation:
            //_anim.Play("enemydeathanimation");
        }
    }

}
