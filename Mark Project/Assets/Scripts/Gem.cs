﻿using UnityEngine;
using System.Collections;

public class Gem : ScorePickups {

    [SerializeField]
    [Range (10,100)] private int _gemValue;



    //when player collides with gem, add value to score
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.coinPickupSound;
            player.outsideSounds.Play();
            AddScore(_gemValue);
            Disable();
        }
    }
}
