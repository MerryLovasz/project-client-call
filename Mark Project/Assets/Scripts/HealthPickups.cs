﻿using UnityEngine;
using System.Collections;

public class HealthPickups : Pickups {

    //can change how much health is added
    [SerializeField]
    [Range(1, 10)]
    private int _healthValue;

    //reference player value script
    private PlayerValues _pVScript;

    void Start()
    {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
    }

    //when player collides with health and their health lowered, add health
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player) && _pVScript.Health < _pVScript.maxHealth)
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.healthPickupSound;
            player.outsideSounds.Play();
            //add health
            _pVScript.Damage(-_healthValue);
            //remove pickup
            Disable();
        }
    }
}
