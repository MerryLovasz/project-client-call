﻿using UnityEngine;
using System.Collections;

public static class Layers {

    public static int Default = 0;
    public static int TransparentFX = 1;
    public static int IgnoreRaycast = 2;
    public static int Water = 4;
    public static int UI = 5;
    public static int BulletWhitelist = 8;
    public static int Bullet = 9;
    public static int Ground = 10;
}