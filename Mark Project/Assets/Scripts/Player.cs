﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    //Plaver variables
    //Health 0-100
    //Score
    //Has Rum

    private int health;
    private int score;
    private bool hasRum;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void DamagePlayer(int damage)
    {
        health -= damage;
        //TODO: apply feedback
    }

    public void AddScore(int value)
    {
        score += value;
        //TODO: apply feedback
    }

    public int Health
    { get { return health; } }

    public int Score
    { get { return score; } }
}
