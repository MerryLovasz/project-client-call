﻿using UnityEngine;
using System.Collections;

public class PlayerMoveScript : MonoBehaviour
{
    [SerializeField]
    private float _speed;

    [SerializeField]
    private Rigidbody _rigidbody;

    [HideInInspector]
    public static bool canWalk;

    void FixedUpdate()
    {
        //if (canWalk) {

            if (Input.GetKey(KeyCode.D)) {
                _rigidbody.AddForce(transform.right * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.A)) {
                _rigidbody.AddForce(transform.right * -_speed * Time.deltaTime);
            }
            else {
                _rigidbody.angularVelocity = Vector3.zero;
            }
        //}

    }

    void SetTransformX(float n)
    {
        if (canWalk)
        {
            transform.position = new Vector3(n, transform.position.y, transform.position.z);
        }
    }
}