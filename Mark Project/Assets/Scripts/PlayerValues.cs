﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerValues : MonoBehaviour
{
    //UI
    private Slider healthSlider;
    private Text TotalScore;
    private Text BallCount;
    private Text LivesText;
    private Text KeysText;
    private Image SwordImage;
    private GameObject CannonballImage;
    [HideInInspector]
    public UISpriteSwapCannonball SpriteSwap;
    //Values
    [Range(0f, 100f)]
    [SerializeField]
    private int _health;
    [HideInInspector]
    public int maxHealth;
    public int Cannonballs;
    public int ExplosiveCannonballs;
    [HideInInspector]
    public int Score = 0;
    [HideInInspector]
    public int Keys = 0;
    //checkpoints
    public GameObject[] _spawnArea = new GameObject[4];
    [HideInInspector]
    public int _checkpoint = 0;
    private GameObject _player;
    private PlayerControls pControls;
    private Animator _anim;
    [SerializeField]
    //damage feedback start
    private float _collisionTimer = 0.0f;
    [SerializeField]
    private float _collisionTimerMax = 2.0f;
    private bool _canCollideWEnemy = true;
    private Collider _enemyCollider;

    private Renderer[] _renderers;
    [SerializeField]
    private GameObject _playerModel;
    [SerializeField]
    private float _flickerInterval = 0.125f;
    private float _canFlickerTimer = 0.0f;
    private float _flickerTimer = 0.0f;
    private float _flickerTimerMax = 2.0f;
    private bool _canFlicker = false;
    private bool _meshREnabled = true;
    //damage feedback end


    //Reference
    private PlayerManager pManager;

    // Use this for initialization
    private void Start()
    {
        _player = this.gameObject;
        pControls = GetComponent<PlayerControls>();
        pManager = FindObjectOfType<PlayerManager>();
        _anim = GetComponentInChildren<Animator>();

        //UI
        healthSlider = GameObject.Find("Health").GetComponent<Slider>();
        TotalScore = GameObject.Find("ScoreText").GetComponent<Text>();
        BallCount = GameObject.Find("CanonBallsText").GetComponent<Text>();
        LivesText = GameObject.Find("Lives").GetComponent<Text>();
        KeysText = GameObject.Find("KeyAmountText").GetComponent<Text>();
        SwordImage = GameObject.Find("sword image").GetComponent<Image>();
        CannonballImage = GameObject.Find("Cannonball Image");
        if (pManager.ScorePlayer != 0)
        {
            Score = pManager.ScorePlayer;
        }
        maxHealth = _health; //set maxhealth to health when the level starts
        healthSlider.maxValue = maxHealth; //set slider to correct max value
        SpriteSwap = FindObjectOfType<UISpriteSwapCannonball>();


        _renderers = GetComponentsInChildren<Renderer>();
    }

    public void Damage(int pDamage)
    {
        if (pManager.LivesPlayer > 0) {
            if (_canFlicker == false) {
                PlayerControls player = GetComponent<PlayerControls>();

                if (pDamage >= _health) {
                    Debug.Log("Player got hit by a fatal hit!");
                    pManager.LoseLife();
                    _player.transform.position = _spawnArea[_checkpoint].transform.position;
                    _health = maxHealth;
                    _anim.Play("dying");
                }
                else if (pDamage < _health && pDamage > 0) {
                    player.outsideSounds.clip = player.hitSound;
                    player.outsideSounds.Play();
                    _health -= pDamage;
                    _canFlicker = true;
                    _anim.Play("get hit");
                    Debug.Log("Player got damaged for " + pDamage);
                }
            }
        }
        else {
            //show death scene and go back to menu
            Debug.Log("Player should be dead; lives not handled yet.");
        }
    }
    public void IgnoreColl(bool pBool, Collider pEnemy)
    {
        _enemyCollider = pEnemy;
        Physics.IgnoreCollision(GetComponent<CapsuleCollider>(), _enemyCollider, pBool);
        if (pBool == true)
        {
            _canCollideWEnemy = false;
        }

    }

    private void ChangeMeshR(bool pBool)
    {
        foreach (Renderer item in _renderers)
        {
            item.enabled = pBool;
        }
        if (pControls.HasSword == false)
        {
            pControls.ActivateSword(false, false);
        }
        if (pControls.HasCannon == false)
        {
            pControls.ActivateCannon(false, false);

        }
        _meshREnabled = pBool;
    }

    private void Update()
    {
        if (_canCollideWEnemy == false)
        {
            _collisionTimer += Time.deltaTime;

            if (_collisionTimer >= _collisionTimerMax)
            {
                if (_enemyCollider != null)
                {
                    IgnoreColl(false, _enemyCollider); 
                }
                _canCollideWEnemy = true;
                _collisionTimer = 0.0f;
            }
        }
        if (_canFlicker)
        {
            _canFlickerTimer += Time.deltaTime;
            _flickerTimer += Time.deltaTime;
            if (_flickerTimer >= _flickerInterval)
            {
                ChangeMeshR(!_meshREnabled);
                _flickerTimer = 0.0f;
            }
            if (_canFlickerTimer >= _flickerTimerMax)
            {
                _canFlickerTimer = 0.0f;
                _flickerTimer = 0.0f;
                ChangeMeshR(true);
                _meshREnabled = true;
                _canFlicker = false;
            }
        }
        if (Score != 0)
            pManager.ScorePlayer = Score;
        //update slider
        healthSlider.value = _health;
        //updte UI
        TotalScore.text = "" + Score;
        KeysText.text = "" + Keys;
        SwordImage.enabled = pControls.HasSword;
        CannonballImage.SetActive(pControls.HasCannon);
        switch (pControls.CurrentProj)
        {
            case PlayerControls.CurrentProjectile.Normal:
                BallCount.text = "" + Cannonballs;
                SpriteSwap.ExplosiveEquipped = false;
                break;
            case PlayerControls.CurrentProjectile.Explosive:
                BallCount.text = "" + ExplosiveCannonballs;
                SpriteSwap.ExplosiveEquipped = true;
                break;
        }
        LivesText.text = "" + pManager.LivesPlayer;

        //Wait for the death animation to finish, before you can move again.
        //if (_playerDied == true)
        //{
        //    pControls.SetPlayerWalkable(false);
        //    if (!GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).IsName("dying"))
        //    {
        //        pControls.SetPlayerWalkable(true);
        //        _playerDied = false;
        //    }
        //}
            if (_anim.GetCurrentAnimatorStateInfo(0).IsName("dying"))
            {
                pControls.SetPlayerWalkable(false);
                GetComponent<Rigidbody>().velocity = Vector3.zero;
            } else
            {
                pControls.SetPlayerWalkable(true);
            }

    }

    //getter/setter for health
    public int Health
    {
        get { return _health; }
        set
        {
           _health = value;
        }
    }

    public Collider EnemyCollider { get { return _enemyCollider; } set { _enemyCollider = value; } }
}
