﻿using UnityEngine;
using System.Collections;

public class ScorePickups : Pickups {

    //reference to player value script
    protected PlayerValues _pVScript;

    private void Start()
    {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
    }

	protected void AddScore(int _score)
    {
        _pVScript.Score += _score;
    }
}
