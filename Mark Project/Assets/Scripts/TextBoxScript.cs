﻿using UnityEngine;
using UnityEngine.UI;

public class TextBoxScript : MonoBehaviour {

    private Image _textBox;
    private Text _text;
    [SerializeField]
    private string _message;
    private bool _on = false;

    private string[] _menuButtons = new string[1] {
        "Continue"
    };

    private int _selectedIndex = 0;

    // Use this for initialization
    void Start () {
        _textBox = GameObject.Find("TextBoxImage").GetComponent<Image>();
        _textBox.gameObject.SetActive(false);
        _text = _textBox.GetComponentInChildren<Text>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = FindObjectOfType<PlayerControls>();
            player.outsideSounds.clip = player.textPopupSound;
            player.outsideSounds.Play();
            _textBox.gameObject.SetActive(true);
            _text.text = _message;
            Time.timeScale = 0.0f;
            _on = true;
        }
    }
    
    public void ContinueGame()
    {
        Time.timeScale = 1.0f;
        _textBox.gameObject.SetActive(false);
    }

    private void OnGUI()
    {
        if(_on == true)
        {
            GUI.SetNextControlName("Continue");
            if (GUI.Button(new Rect(Screen.width / 2.3f, Screen.height / 2, Screen.height / 3, Screen.width / 40), "Continue"))
            {
                ContinueGame();
                _on = false;
            }
            GUI.FocusControl(_menuButtons[_selectedIndex]);
        }
    }
}
