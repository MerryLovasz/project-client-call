﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UISpriteSwapCannonball : MonoBehaviour
{

    public Sprite cannonball;
    public Sprite explosiveCannonball;

    public bool equipExCannonBall = false;

    void Update()
    {
        if (equipExCannonBall == true)
        {
            transform.GetComponent<Image>().sprite = explosiveCannonball;
        }
        else
        {
            transform.GetComponent<Image>().sprite = cannonball;
        }
    }

    public bool ExplosiveEquipped { get { return equipExCannonBall; } set { equipExCannonBall = value; } }
}
