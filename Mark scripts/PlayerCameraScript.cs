﻿using UnityEngine;
using System.Collections;

public class PlayerCameraScript : MonoBehaviour {

    [SerializeField]
    private Transform _target, _startClamp, _endClamp;
    [Range(0, 200)] [SerializeField]
    private float _smoothing = 5f, _clampAddition;
    private Vector3 _offset, _targetPos;

	void Start () {
        _offset = transform.position - _target.position;
	}
	
	void Update () {
        if (!_target) {
            Debug.Log("Player reference missing in PlayerCameraScript.cs");
            return;
        }

        
        _targetPos = _target.position + _offset;        //add a smoothing factor.
        //This makes sure the camera doesn't move when the _clamp gameObjects are in range.
        //This creates a start and an end to every level.
        transform.position = Vector3.Lerp(transform.position, new Vector3(_targetPos.x, _targetPos.y, Mathf.Clamp(_targetPos.z, _startClamp.position.z + _clampAddition, _endClamp.position.z - _clampAddition)), _smoothing * Time.deltaTime);
	}
}
