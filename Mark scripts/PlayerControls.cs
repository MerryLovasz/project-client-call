﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour
{
    //canDamage says if the weapon can give damage (true when anim is playing)
    [HideInInspector]
    public bool canDamage = false;

    private Animator anim;
    private Rigidbody _rigidbody;
    private GameObject currentPlatform;

    private bool _canWalk = true, _grounded = false, _canDoubleJump = false;
    private Vector3 _moveDirection;
    private CharacterController _controller;

    private Collider[] _groundCollisions;
    private float _groundCheckRadius = 0.01f;
    public LayerMask groundLayer;
    public Transform groundCheck;

    [Header("Lock & Hide the Mousecursor?")]
    [SerializeField]
    private bool _lockMouseCursor = false;

    [Header("Player Attributes")]
    [SerializeField]
    [Range(0f, 100f)]
    private int _health = 100;
    [SerializeField]
    private float _speed, _jumpheight;
    [Range(0f, 100f)]
    public int swordDamage = 50;
    [SerializeField]
    private GameObject _bullet;
    [SerializeField]
    private float _bulletSpeed;
    [SerializeField]
    private int _bulletDamage;

    void Start()
    {
        anim = GetComponent<Animator>(); // reference the animator in the weapon.
        _rigidbody = GetComponent<Rigidbody>();
        _controller = GetComponent<CharacterController>();

        if (_lockMouseCursor) {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void AttackSword()
    {
        anim.Play("attacksword");
        Debug.Log("Animation Playing: attacksword in Player.");
    }

    private void AttackGun()
    {
        Quaternion rot = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z);
        Vector3 pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        //GameObject bullet = Instantiate(_bullet, pos, rot, gameObject.transform) as GameObject;
        GameObject bullet = Instantiate(_bullet);
        bullet.GetComponent<ProjectileScript>().GetDamage(_bulletDamage);
        bullet.transform.position = pos;
        bullet.GetComponent<Rigidbody>().AddForce((transform.transform.forward * _bulletSpeed * Time.deltaTime) * 100, ForceMode.Impulse);
    }
    private void SwitchLook(float pRotation)
    {
        Quaternion rot = transform.rotation;
        rot = new Quaternion(rot.x, pRotation, rot.z, rot.w);
        transform.rotation = rot;
        //Debug.Log(rot);
        //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(transform.position - _prevLoc), Time.fixedDeltaTime * _lookSpeed);
        //transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.fixedDeltaTime * _lookSpeed);
    }

    private void FixedUpdate()
    {
        RuleBook();

        _moveDirection = new Vector3(0, 0, Input.GetAxis("Horizontal"));
        if (_canWalk) {
            //_prevLoc = _curLoc;
            //_curLoc = transform.position;
            if (_moveDirection.z > 0f) {
                SwitchLook(0f);
            }
            else if (_moveDirection.z < 0f) {
                SwitchLook(180f);
            }

        }
        if (Input.GetButtonDown("Jump")) {
            //check for collisions with a sphere under the player vs the ground layer.
            _groundCollisions = Physics.OverlapSphere(groundCheck.position, _groundCheckRadius, groundLayer);
            if (_groundCollisions.Length > 0) {
                _grounded = true;
            }
            else {
                _grounded = false;
            }

            if (!_grounded) {
                if (_canDoubleJump) {
                    _rigidbody.AddForce((transform.up * _jumpheight * Time.deltaTime) * 100, ForceMode.Impulse);
                    _canDoubleJump = false;
                }
            }
            if (_grounded) {
                _rigidbody.AddForce((transform.up * _jumpheight * Time.deltaTime) * 100, ForceMode.Impulse);
                _canDoubleJump = true;
            }
        }

        else {
            _rigidbody.angularVelocity = Vector3.zero;
        }

        //Update the velocity here
        _moveDirection *= _speed;
        _rigidbody.velocity = new Vector3(_moveDirection.x, _rigidbody.velocity.y, _moveDirection.z);

        if (Input.GetKeyDown(KeyCode.LeftControl)) {
            AttackSword();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift)) {
            AttackGun();
        }
    }
    
    public void SetPlayerWalkable(bool pCanWalk)
    {
        _canWalk = pCanWalk;
    }

    private void RuleBook()
    {
        //RuleBook is a collection of statements that should be in update.
        //Note: Kind of an old concept, could be replaced/moved.
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("attacksword")) {
            canDamage = true;
        }
        else {
            canDamage = false;
        }
    }
    
    public GameObject GetPlatform()
    {
        return currentPlatform;
    }

    public int Health
    {
        get { return _health; }
        set { _health = value; }
    }

}