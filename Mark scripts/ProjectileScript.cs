﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {

    private float _speed;
    private float _damage;

    public void GetDamage(int pDamage)
    {
        _damage = pDamage;
    }

    public void GetSpeed(int pSpeed)
    {
        _speed = pSpeed;
    }
}