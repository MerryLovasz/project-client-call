﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneloaderScript : MonoBehaviour
{
    [SerializeField]
    private Texture2D _fadeOutTexture;                  //texture on transition
    [SerializeField]
    private int _sceneNumber;                           //next scene to be loaded
    [SerializeField]
    private float _fadeSpeed = 0.8f;                    //speed of the fade in/out


    private int _drawDepth = -1000, _fadeDir = -1;      //direction to fade: in = -1 | out = 1
    private float _alpha = 1.0f;                        //the alpha value of the texture

    public void LoadScene(int sceneBuildIndex)
    {
        SceneManager.LoadScene(sceneBuildIndex);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadSceneAdditive(int sceneBuildIndex)
    {
        SceneManager.LoadScene(sceneBuildIndex, LoadSceneMode.Additive);
    }

    public void LoadSceneAdditive(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            float fadeTime = BeginFade(1);
            StartCoroutine(ChangeLevelWithDelay(fadeTime));
        }
    }

    IEnumerator ChangeLevelWithDelay(float pTime)
    {
        if (pTime == 0) {
            yield break;
        }
        yield return new WaitForSeconds(pTime);
        LoadScene(_sceneNumber);
    }

    private void OnGUI()
    {
        // fade out/in the alpha value using a direction, a speed and Time.deltatime to convert to seconds.
        _alpha += _fadeDir * _fadeSpeed * Time.deltaTime;
        //clamp the alpha between numbers 0 and 1
        _alpha = Mathf.Clamp01(_alpha);

        //setup the texture
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, _alpha);
        GUI.depth = _drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _fadeOutTexture);      //fill the screen with our texture
    }

    //this sets _fadeDir to the parameter, making the scene fade in or out.
    public float BeginFade(int pDirection)
    {
        _fadeDir = pDirection;
        return (_fadeSpeed);        //return the speed, so it's easy to time the loading of the next level
    }
    //called when a level is loaded. 
    public void OnLevelWasLoaded()
    {
        BeginFade(-1);              //reverses the fading, so now the texture fades out
    }
}
