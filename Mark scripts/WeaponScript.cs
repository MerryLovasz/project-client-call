﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour {

    private Animator anim;
    private PlayerControls _player;
    private int _swordDamage;
    private bool _canDamage;

    private void Start()
    {
        _player = GetComponentInParent<PlayerControls>();
        anim = GetComponentInParent<Animator>();
        _swordDamage = _player.swordDamage;
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("Enemy")) {
            _canDamage = _player.canDamage;
            if (_canDamage) {
                c.gameObject.GetComponent<EnemyScript>().GetDamage(_swordDamage);
                Debug.Log("Player hit: Enemy");
            }
        }
    }

    private void OnTriggerExit(Collider c)
    {
        if (c.CompareTag("Enemy")) {
            Debug.Log("Collision with enemy lifted");
        }
    }
}
