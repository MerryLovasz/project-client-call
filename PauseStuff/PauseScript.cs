﻿using UnityEngine;

public class PauseScript : MonoBehaviour
{
    [SerializeField]
    private GameObject _pauseCanvas;
    private bool _paused = false;
    private PlayerControls _player;
    [SerializeField]
    private SceneloaderScript _sceneLoader;
    

    void Awake()
    {
       _player = FindObjectOfType<PlayerControls>();
       _pauseCanvas.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButtonDown("Pause"))
            _paused = togglePause();

        if (_paused)
        {
            _player.SetPlayerWalkable(false);
            if (Input.GetButtonDown("Fire1"))
                _sceneLoader.LoadScene(0);
        }
        else if (!_paused)
        {
            _player.SetPlayerWalkable(true);
        }

        print("Game paused: " + _paused);
    }

    public bool togglePause()
    {
        if (_paused == true)
        {
            _pauseCanvas.SetActive(false);
            Time.timeScale = 1f;
            return (false);
        }
        else
        {
            _pauseCanvas.SetActive(true);
            Time.timeScale = 0f;
            return (true);
        }

    }
}