﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(LevelEditor))]
public class LevelEditorInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LevelEditor myScript = (LevelEditor)target;
        if (GUILayout.Button("Save Level"))
        {
            myScript.Save();
        }
        if (GUILayout.Button("Load Level"))
        {
            myScript.Read();
        }
    }
}