﻿using UnityEngine;

public class AmmoPickups : Pickups {

    //reference to player values
    protected PlayerValues _pVScript;

    //use this for initialization
    private void Start()
    {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
    }

    /// <summary>
    /// Function that adds the picked up ammo to the inventory
    /// </summary>
    /// <param name="ammoValue"></param>
    protected void AddAmmo(int ammoValue)
    {
        _pVScript.Cannonballs += ammoValue;
    }
}
