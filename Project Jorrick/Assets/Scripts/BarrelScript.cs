﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the Barrel pickup.
/// </summary>
public class BarrelScript : MonoBehaviour
{
    [SerializeField]
    private GameObject _healthPickup;
    [SerializeField]
    private GameObject _pickupLocation;
    [SerializeField]
    //Does the barrel drop a pickup?
    private bool _dropsPickup = true;

    // Use this for initialization
    private void Start()
    {
        _healthPickup.SetActive(false);
    }


    //Instantiates a pickup at the location of _pickupLocation
    //In this case we spawn a health pickup
    public void DoDestroy()
    {
        if (_dropsPickup == true)
        {
            _healthPickup.SetActive(true);
            GameObject healthPickup = Instantiate(_healthPickup);
            healthPickup.transform.position = _pickupLocation.transform.position;
        }
        Destroy(this.gameObject);
    }

    public void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag(Tags.Projectile))
        {
            ProjectileScript projectile = c.GetComponent<ProjectileScript>();
            if (projectile.ProjOwner == ProjectileScript.Owner.Player)
            {
                DoDestroy();
            }
        }
    }

}
