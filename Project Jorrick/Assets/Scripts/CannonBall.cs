﻿using UnityEngine;

public class CannonBall : AmmoPickups {

    private int _cannonValue = 1;

    /// <summary>
    ///when player collides with cannonball, add ball to inventory
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.pickupCannonSound;
            player.outsideSounds.Play();
            Disable();
            AddAmmo(_cannonValue);
        }
    }
}
