﻿using UnityEngine;

public class CheckpointScript : MonoBehaviour
{
    private GameObject _player;
    private Vector3 _playerCurrentPos;
    private Quaternion _playerCurrentRot;

    private GameObject _enemy;
    private GameObject[] _enemyArray;
    private Vector3[] _enemyPosArray;
    private Quaternion[] _enemyRotArray;

    private GameObject _pillow;
    private GameObject[] _pillowArray;
    private Vector3[] _pillowPosArray;
    private Quaternion[] _pillowRotArray;

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");

        _enemyArray = GameObject.FindGameObjectsWithTag("Agent");
        _enemyPosArray = new Vector3[_enemyArray.Length];
        _enemyRotArray = new Quaternion[_enemyArray.Length];            

        _pillowArray = GameObject.FindGameObjectsWithTag("pickup");
        _pillowPosArray = new Vector3[_pillowArray.Length];
        _pillowRotArray = new Quaternion[_pillowArray.Length];
    }

    public void RestorePositions()
    {
        /// Player ///
        _player.transform.position = _playerCurrentPos;
        _player.transform.localRotation = _playerCurrentRot;

        /// Enemies ///
        for (int i = 0; i < _enemyArray.Length; i++)
        {
            _enemy = _enemyArray[i];
            _enemy.transform.position = _enemyPosArray[i];
            _enemy.transform.localRotation = _enemyRotArray[i];
        }

        /// Pillows ///
        for (int i = 0; i < _pillowArray.Length; i++)
        {
            _pillow = _pillowArray[i];
            _pillow.transform.position = _pillowPosArray[i];
            _pillow.transform.localRotation = _pillowRotArray[i];
        }
    }

    public void StorePositions()
    {
        /// Player ///
        _playerCurrentPos = _player.transform.position;
        _playerCurrentRot = _player.transform.localRotation;

        /// Enemies ///
        for (int i = 0; i < _enemyArray.Length; i++)
        {
            _enemy = _enemyArray[i];
            _enemyPosArray[i] = _enemy.transform.position;
            _enemyRotArray[i] = _enemy.transform.localRotation;
        }

        /// Pillows ///
        for (int i = 0; i < _pillowArray.Length; i++)
        {
            _pillow = _pillowArray[i];
            _pillowPosArray[i] = _pillow.transform.position;
            _pillowRotArray[i] = _pillow.transform.localRotation;
        }
    }
}