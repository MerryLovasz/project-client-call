﻿using UnityEngine;

public class Coin : ScorePickups {

    [SerializeField]
    [Range (1,10)] private int _coinValue; //can alter the value in the inspector

    /// <summary>
    /// when player collides with coin, add it to the score
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.coinPickupSound;
            player.outsideSounds.Play();
            AddScore(_coinValue);
            //TODO: play sound/ anim(implement)
            Disable();
        }
    }
}
