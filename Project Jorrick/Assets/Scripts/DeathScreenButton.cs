﻿using UnityEngine;

public class DeathScreenButton : MonoBehaviour
{
    //get the reference
    [SerializeField]
    private SceneloaderScript _sceneLoader;
    private PlayerManager pManager;
    //the button
    private string[] _menuButtons = new string[1] {
        "Back"
    };
    private int _selectedIndex = 0;

    // Use this for initialization
    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneloaderScript>();
    }
    
    private void Update()
    {
        //make sure the manager isn't null
        if (pManager == null)
        {
            pManager = PlayerManager.instance; 
        }
    }

    /// <summary>
    /// Create the button and apply it's function
    /// </summary>
    private void OnGUI()
    {
        GUI.SetNextControlName("Back");
        if (GUI.Button(new Rect(Screen.width / 2.3f, Screen.height / 2, Screen.height / 3, Screen.width / 40), "Back to Menu"))
        {
            Time.timeScale = 1.0f;
            pManager.ScorePlayer = 0;
            pManager.LivesPlayer = 3;
            pManager.FoundDeath = false;
			_sceneLoader.LoadScene(0); //back to menu
        }

        GUI.FocusControl(_menuButtons[_selectedIndex]);
    }
}

