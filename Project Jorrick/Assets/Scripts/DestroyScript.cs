﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DestroyScript : MonoBehaviour {

    [SerializeField]
    private GameObject _wall;
    [SerializeField]
    private Text _help;

    private  PlayerValues _pVScript;

    // Use this for initialization
    private void Start () {
        _pVScript = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerValues>();
        _help.gameObject.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(Tags.Player) && _pVScript.ExplosiveCannonballs >= 1) 
        {
            _help.gameObject.SetActive(true);
            DestroyWall();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _help.gameObject.SetActive(false);
    }

    private void DestroyWall()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //play animation and sound
            _wall.SetActive(false);
            _pVScript.ExplosiveCannonballs -= 1;
            _help.gameObject.SetActive(false);
            this.gameObject.SetActive(false);

        }
    }
}
