﻿using UnityEngine;
using System.Collections;
/// <summary>
///Handles the destructible Door object. 
/// </summary>
public class DoorScript : MonoBehaviour {
    [SerializeField]
    private int _doorHealth = 200;
    
    public void Update ()
    {
        if (_doorHealth <= 0)
        {
            OpenDoor();
        }
    }
    public void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag(Tags.Projectile))
        {
            ProjectileScript projectile = c.GetComponent<ProjectileScript>();
            if (projectile.ProjOwner == ProjectileScript.Owner.Player)
            {
                Debug.Log(projectile.Damage);
                _doorHealth -= projectile.Damage;
            }
        }
        else if (c.CompareTag(Tags.Sword))
        {
            WeaponScript sword = c.GetComponent<WeaponScript>();
            PlayerControls pControl = sword.GetComponentInParent<PlayerControls>();
            if (pControl.canDamage == true)
            {
                _doorHealth -= sword.SwordDamage; 
            }
        }
    }

    public void OpenDoor()
    {
        //play animation
        Destroy(this.gameObject);
    }
}
