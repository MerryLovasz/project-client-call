﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private int _health;
    [SerializeField]
    private int _meleeDamage;
    [SerializeField]
    private int _rangedDamage;
    [SerializeField]
    private float _walkSpeed;
    [SerializeField]
    private float _bulletSpeed;
    [SerializeField]
    private float _attackCooldown;
    [SerializeField]
    private float _attackTimer;
    [SerializeField]
    private float _meleeDistance;
    [SerializeField]
    private float _rangedDistance;
    [SerializeField]
    private float _playerDelta;
    [SerializeField]
    private bool _canAttack;
    [SerializeField]
    private float _score;
    [SerializeField]
    private List<Transform> _waypoints = new List<Transform>();
    [SerializeField]
    private GameObject _bullet;
    [SerializeField]
    private bool _isMeleeing = false;

    private NavMeshAgent _navAgent;

    [SerializeField]
    private AttackType _combatType;
    [SerializeField]
    private State _currentState;
    private State _beginState;
    private PlayerControls _pControl;
    private GameObject _player, _currentPlatform;
    private Animator _anim;
    private int _currentWaypoint;
    private float _yRot;

    private enum AttackType
    {
        Melee, Ranged, Both
    }
    private enum State
    {
        Idle, Patrol, Attack, Dead
    }

    public void Start()
    {
        _navAgent = GetComponent<NavMeshAgent>();
        _navAgent.speed = _walkSpeed;
        _navAgent.updateRotation = false;
        _currentWaypoint = 0;
        _beginState = _currentState;
        _player = FindObjectOfType<PlayerControls>().gameObject;
        _pControl = _player.GetComponent<PlayerControls>();
        _anim = GetComponent<Animator>();
        //Always SwitchState() last!
        //Null references are bound to come up otherwise.
        SwitchState(_currentState);
    }

    void Update()
    {
        if (_anim == null)
        {
            Debug.Log("enemy anim is null");
            return;
        }
        //distance between player and the enemy
        if (_player == null)
        {
            _player = FindObjectOfType<PlayerControls>().gameObject;
            _pControl = _player.GetComponent<PlayerControls>();
        }
        else {
            _playerDelta = Vector3.Distance(transform.position, _player.transform.position);
        }
        checkState();

    }
    private void checkState()
    {
        if (_currentState == State.Patrol)
        {
            _yRot = (_navAgent.velocity.z > 0.0f) ? 0.0f : 180.0f;
            setRot();
            //Waypoints
            if (_navAgent.remainingDistance < 0.25f)
            {
                _currentWaypoint++;
                if (_currentWaypoint > _waypoints.Count - 1) _currentWaypoint = 0;
                _navAgent.destination = _waypoints[_currentWaypoint].position;
            }
            //check for the combat type
            switch (_combatType)
            {
                case AttackType.Melee:
                    if (doRayCast() == true && _playerDelta <= _meleeDistance)
                    {
                        SwitchState(State.Attack);
                    }
                    break;
                case AttackType.Ranged:
                    if (doRayCast() == true && _playerDelta <= _rangedDistance)
                    {
                        SwitchState(State.Attack);
                    }
                    break;
                case AttackType.Both:
                    if (doRayCast() == true && _playerDelta <= _rangedDistance)
                    {
                        SwitchState(State.Attack);
                    }
                    break;
                default:
                    break;
            }


            if (doRayCast() == true && _playerDelta <= _meleeDistance)
            {
                SwitchState(State.Attack);
            }


        }
        else if (_currentState == State.Idle)
        {
            //can I see the player and is the player in the range distance?
            if (doRayCast() == true && _playerDelta <= _rangedDistance)
            {
                switch (_combatType)
                {
                    case AttackType.Melee:
                        if (_playerDelta <= _meleeDistance)
                        {
                            SwitchState(State.Attack);
                        }
                        break;
                    case AttackType.Ranged:
                        SwitchState(State.Attack);
                        break;
                    case AttackType.Both:
                        SwitchState(State.Attack);
                        break;
                    default:
                        break;
                }
            }
        }
        if (_currentState == State.Attack)
        {
            _yRot = ((_player.transform.position.z - transform.position.z) > 0.0f) ? 0.0f : 180.0f;

            setRot();
            if (_canAttack == true)
            {
                attack();
                _canAttack = false;
            }
            else if (_canAttack == false)
            {
                _attackTimer += Time.deltaTime;

                if (_attackTimer >= _attackCooldown)
                {
                    _attackTimer = 0;
                    _canAttack = true;
                }
            }
        }
    }
    private bool doRayCast()
    {
        RaycastHit hit;
        Vector3 rayDirection = (_player.transform.position - transform.position).normalized;
        if (Physics.Raycast(transform.position, rayDirection, out hit))
        {
            if (hit.collider.gameObject.CompareTag(Tags.Player))
            {
                return true;
            }
        }
        else
        {
            Debug.Log(gameObject.name + " cannot see player.");
            return false;
        }
        return false;
    }
    private void SwitchState(State pState)
    {
        switch (pState)
        {
            case State.Idle:
                _anim.Play("idle");
                Debug.Log(gameObject.name + " Changed to idle state");
                _navAgent.Stop();
                break;
            case State.Patrol:
                _anim.Play("walking");
                Debug.Log(gameObject.name + " Changed to patrol state");
                _navAgent.Resume();
                break;
            case State.Attack:
                Debug.Log(gameObject.name + " Changed to attack state");
                _navAgent.Stop();
                break;
            case State.Dead:
                Debug.Log(gameObject.name + " Changed to dead state");
                _navAgent.Stop();
                dying();
                break;
        }
        _currentState = pState;
    }

    private void dying()
    {
        //On dying, the enemy will remain in the level & without the collider.
        _anim.Play("dying");
        GetComponent<Collider>().enabled = false;
    }

    private void attack()
    {
        //look for the combattype of the enemy (Melee/Ranged)
        switch (_combatType)
        {
            case AttackType.Melee:
                if (_playerDelta <= _meleeDistance)
                {
                    melee();
                }
                else {
                    SwitchState(_beginState);
                }
                break;
            case AttackType.Ranged:
                if (_playerDelta <= _rangedDistance)
                {
                    shoot();
                }
                else {
                    SwitchState(_beginState);
                }
                break;
            case AttackType.Both:
                //if player distance is inbetween shooting distance and melee distance --> shoot()
                if (_playerDelta <= _rangedDistance && _playerDelta > _meleeDistance)
                {
                    shoot();
                }
                //if player distance if in shooting AND melee distance --> melee
                else if (_playerDelta <= _rangedDistance && _playerDelta <= _meleeDistance)
                {
                    melee();
                }
                else if (_playerDelta > _rangedDistance)
                {
                    SwitchState(State.Patrol);

                }
                break;
        }
    }
    private void melee()
    {
        //melee looks if the animation isnt already playing. This will give a better feel to the game. 
        //This method damages the player.
        if (!_anim.GetCurrentAnimatorStateInfo(0).IsName("get hit") & !_anim.GetCurrentAnimatorStateInfo(0).IsName("melee attack"))
        {
            PlayerValues pvScript = _player.GetComponent<PlayerValues>();
            pvScript.Damage(_meleeDamage);
            pvScript.IgnoreColl(true, GetComponent<CapsuleCollider>());
            _anim.Play("melee attack");
        }
    }

    private void shoot()
    {
        //melee looks if the animation isnt already playing. This will give a better feel to the game. 
        if (!_anim.GetCurrentAnimatorStateInfo(0).IsName("get hit") && !_anim.GetCurrentAnimatorStateInfo(0).IsName("range attack"))
        {
            Quaternion rot = transform.rotation;
            Vector3 pos = transform.position;
            //Instantiate the projectile prefab you want
            GameObject projectile = Instantiate(_bullet);
            ProjectileScript projScript = projectile.GetComponent<ProjectileScript>();
            //Give the projectile an owner, in this case: Enemy
            projScript.ProjOwner = ProjectileScript.Owner.Enemy;
            projScript.Damage = _rangedDamage;
            projectile.transform.position = pos + transform.transform.forward;
            //move the bullet & play the animation.
            projectile.GetComponent<Rigidbody>().AddForce((transform.transform.forward * _bulletSpeed * Time.deltaTime) * 100, ForceMode.Impulse);
            _anim.Play("range attack");
        }
    }

    private void setRot()
    {
        Quaternion rot = transform.rotation;
        rot = new Quaternion(rot.x, _yRot, rot.z, rot.w);
        transform.rotation = rot;
    }
    public void Damage(int damage)
    {
        if (damage >= _health)
        {
            SwitchState(State.Dead);
        }
        else
        {
            _health -= damage;
            _anim.Play("get hit");
        }
    }
    #region Props
    public int Health
    {
        get { return _health; }
        set { _health = value; }
    }

    public int MeleeDamage
    {
        get { return _meleeDamage; }
        set { _meleeDamage = value; }
    }
    public int RangedDamage
    {
        get { return _rangedDamage; }
        set { _rangedDamage = value; }
    }
    public float WalkSpeed
    {
        get { return _walkSpeed; }
        set { _walkSpeed = value; }
    }

    public float BulletSpeed
    {
        get { return _bulletSpeed; }
        set { _bulletSpeed = value; }
    }

    public float MeleeDistance
    {
        get { return _meleeDistance; }
        set { _meleeDistance = value; }
    }
    public float RangedDistance
    {
        get { return _rangedDistance; }
        set { _rangedDistance = value; }
    }
    #endregion
}
