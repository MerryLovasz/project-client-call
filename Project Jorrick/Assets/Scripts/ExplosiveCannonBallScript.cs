﻿using UnityEngine;

public class ExplosiveCannonBallScript : AmmoPickups
{
    /// <summary>
    /// when player collides with the ball, add to inventory
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerControls player = other.GetComponent<PlayerControls>();
            player.outsideSounds.clip = player.pickupCannonSound;
            player.outsideSounds.Play();
            _pVScript.ExplosiveCannonballs += 1;
            Disable();
        }
    }
}
