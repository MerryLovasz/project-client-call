﻿using UnityEngine;
using System.Collections;

public class ExplosiveCanonBallScript : AmmoPickups
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _pVScript.ExplosiveCannonballs += 1;
            Disable();
        }
    }
}
