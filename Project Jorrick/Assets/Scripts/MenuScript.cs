﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Handles the main menu.
/// </summary>
public class MenuScript : MonoBehaviour
{
    private SceneloaderScript _sceneLoader;
    private PlayerManager _pManager;
    private AudioSource _audioSource;
    private List<Image> _menuImages = new List<Image>();

    private int _currentSelection;
    private int _menuOptions;

    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneloaderScript>();
        _pManager = PlayerManager.instance;
        _pManager.gameObject.SetActive(false);
        _audioSource = GetComponentInChildren<AudioSource>();
        _menuImages.Add(GameObject.Find("StartButton").GetComponent<Image>());
        _menuImages.Add(GameObject.Find("ExitButton").GetComponent<Image>());
        _menuOptions = _menuImages.Count;
        updateHighlight();
    }
    private void LateUpdate()
    {
        processMenu();
    }
    private void processMenu()
    {
        
        //pressed down
        if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0)
        {
            currentSelection++;
        }
        //pressed up
        else if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0)
        {
            currentSelection--;
        }

        if (currentSelection > _menuOptions - 1)
        {
            currentSelection = 0;
        }
        else if (currentSelection < 0)
        {
            currentSelection = _menuOptions - 1;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            submitSelection();
        }
    }
    private void submitSelection()
    {
        switch (_currentSelection)
        {
            case 0:
                GetComponent<AudioSource>().Play();
                _pManager.gameObject.SetActive(true);
                _sceneLoader.LoadScene(1); //start the game
                break;

            case 1:
                GetComponent<AudioSource>().Play();
                Application.Quit(); //quit the game
                break;
        }
    }

    private void updateHighlight()
    {
        //go through all of the buttons
        for (int i = 0; i < _menuImages.Count; i++)
        {
            Image currentText = _menuImages[i];
            //TODO: swap the sprite of the button here
            if (i == currentSelection)
            {
                currentText.color = Color.red;
            }
            else {
                currentText.color = Color.white;
               
            }
        }
    }
    private int currentSelection
    {
        get { return _currentSelection; }
        set
        {
            _currentSelection = value;
            _audioSource.Play();
            updateHighlight();
        }
    }
}