﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class PatrolScript : MonoBehaviour {

    // Reference to NavMeshAgent component
    private NavMeshAgent _agent;

    // Movement Speed
    [SerializeField]
    private float _patrolSpeed = 2.0f;

    // Waypoints
    [SerializeField]
    public Transform[] Waypoints;

    private int _curWaypoint = 0;
    private int _maxWaypoint;

    [SerializeField]
    private float _minWaypointDistance = 0.1f;

    // When the game starts
    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _maxWaypoint = Waypoints.Length - 1;
    }

    // Every frame
    private void Update()
    {
        Patrolling();
    }

    //patrolling the waypoints
    public void Patrolling()
    {
        // Set the ai agents movement speed to patrol speed
        _agent.speed = _patrolSpeed;

        // Create two Vector3 variables, one to buffer the ai agents local position, the other to
        // buffer the next waypoints position
        Vector3 _tempLocalPosition;
        Vector3 _tempWaypointPosition;

        // Agents position (x, set y to 0, z)
        _tempLocalPosition = transform.position;
        _tempLocalPosition.y = 0f;

        // Current waypoints position (x, set y to 0, z)
        _tempWaypointPosition = Waypoints[_curWaypoint].position;
        _tempWaypointPosition.y = 0f;

        // Is the distance between the agent and the current waypoint within the minWaypointDistance?
        if (Vector3.Distance(_tempLocalPosition, _tempWaypointPosition) <= _minWaypointDistance)
        {
            // Have we reached the last waypoint?
            if (_curWaypoint == _maxWaypoint)
                // If so, go back to the first waypoint and start over again
                _curWaypoint = 0;
            else
                // If we haven't reached the Last waypoint, just move on to the next one
                _curWaypoint++;
        }

        // Set the destination for the agent
        // The navmesh agent is going to do the rest of the work
        _agent.SetDestination(Waypoints[_curWaypoint].position);
    }
}
