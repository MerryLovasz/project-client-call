﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour
{

    private Animator anim;
    private Rigidbody _rigidbody;
    private GameObject currentPlatform;
    private PlayerValues pVScript;

    //canDamage says if the weapon can give damage (true when anim is playing)
    [HideInInspector]
    public bool canDamage = false;
    //use this to say if the player can walk/jump or not.
    private bool _canWalk = true, _grounded = true, _canDoubleJump = false, _facesRight = true, _canShoot = true;
    private Vector3 _moveDirection;
    private Collider[] _groundCollisions;
    private float _groundCheckRadius = 0.05f, _timerInBetweenJumps = 0.18f, _refJumpTimer, _shootTimer;
    public LayerMask groundLayer;
    public Transform groundCheck;

    [Header("Player Attributes")]
    [SerializeField]
    private bool _activateDoubleJump = true;
    [SerializeField]
    private bool _hasSword = true, _hasCannon = true;
    [SerializeField]
    private bool _lockMouseCursor = false;
    [SerializeField]
    private float _speed, _jumpheight;
    [Range(0f, 100f)]
    public int swordDamage = 50;
    [SerializeField]
    private GameObject _normalBall;
    [SerializeField]
    private GameObject _fireBall;

    [SerializeField]
    private float _bulletSpeed;
    [SerializeField]
    private int _bulletDamage;
    [SerializeField]
    private int _fireBulletDamage;
    [SerializeField]
    private float _shootCooldown;

    public enum CurrentProjectile
    {
        Normal, Explosive
    }
    [SerializeField]
    private CurrentProjectile _cProjectile;

    [Header("Player Audio")]
    //outsidesounds are for triggers such as coins;
    [SerializeField]
    private AudioSource _playerSounds;
    public AudioSource outsideSounds, levelMusic;
    [SerializeField]
    private AudioClip _cannonSound, _jumpSound, _levelSound, _noAmmoSound;
    public AudioClip coinPickupSound, deathSound, healthPickupSound, hitSound, keySound, loseScreenSound, textPopupSound, pickupCannonSound, swordHitSound, swordDeflectSound, chestSound;

    void Start()
    {
        Time.timeScale = 1.0f;
        pVScript = GetComponent<PlayerValues>();
        anim = GetComponentInChildren<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        _refJumpTimer = _timerInBetweenJumps;
        levelMusic.clip = _levelSound;
        levelMusic.Play();

        if (_lockMouseCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else {
            Cursor.lockState = CursorLockMode.None;
        }

        if (_hasSword)
        {
            ActivateSword(true, true);
        }
        else {
            ActivateSword(false, false);
        }

        if (_hasCannon)
        {
            ActivateCannon(true, true);
        }
        else {
            ActivateCannon(false, false);
        }
    }
    public void ActivateSword(bool pValue, bool pEnabled)
    {
        WeaponScript weapon = GetComponentInChildren<WeaponScript>();
        if (pValue)
        {
            weapon.enabled = true;
            Renderer[] r = weapon.gameObject.GetComponentsInChildren<Renderer>();
            foreach (Renderer re in r)
            {
                re.enabled = true;
            }
            weapon.gameObject.GetComponent<Renderer>().enabled = true;
            _hasSword = pEnabled;
        }
        else {
            weapon.enabled = false;
            Renderer[] r = weapon.gameObject.GetComponentsInChildren<Renderer>();
            foreach (Renderer re in r)
            {
                re.enabled = false;
            }
            weapon.gameObject.GetComponent<Renderer>().enabled = false;
            _hasSword = pEnabled;
        }
    }

    public void ActivateCannon(bool pValue, bool pEnabled)
    {
        GameObject c = GameObject.FindGameObjectWithTag(Tags.Cannon);
        if (pValue)
        {
            c.GetComponent<Renderer>().enabled = true;
            _hasCannon = pEnabled;
        }
        else {
            c.GetComponent<Renderer>().enabled = false;
            _hasCannon = pEnabled;
        }
    }


    private void AttackGun(int pType)
    {
        if (_hasCannon)
        {
            Quaternion rot = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z);
            Vector3 pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            GameObject bullet;
            int damage;
            switch (pType)
            {
                case 0:
                    bullet = Instantiate(_normalBall);
                    damage = _bulletDamage;
                    pVScript.Cannonballs -= 1;
                    break;
                case 1:
                    bullet = Instantiate(_fireBall);
                    damage = _fireBulletDamage;
                    pVScript.ExplosiveCannonballs -= 1;

                    break;
                default:
                    bullet = Instantiate(_normalBall);
                    damage = _bulletDamage;
                    pVScript.Cannonballs -= 1;
                    break;

            }
            ProjectileScript projScript = bullet.GetComponent<ProjectileScript>();
            projScript.ProjOwner = ProjectileScript.Owner.Player;
            switch (pType)
            {
                case 0:
                    projScript.ProjType = ProjectileScript.Type.Normal;
                    break;
                case 1:
                    projScript.ProjType = ProjectileScript.Type.Explosive;

                    break;
                default:
                    projScript.ProjType = ProjectileScript.Type.Normal;
                    break;
            }
            projScript.Damage = damage;
            bullet.transform.position = pos + transform.transform.forward;
            bullet.GetComponent<Rigidbody>().AddForce((transform.transform.forward * _bulletSpeed * Time.deltaTime) * 100, ForceMode.Impulse);
            _playerSounds.clip = _cannonSound;
            _playerSounds.Play();
        }
    }
    private void SwitchLook(float pRotation)
    {
        Quaternion rot = transform.rotation;
        rot = new Quaternion(rot.x, pRotation, rot.z, rot.w);
        transform.rotation = rot;
    }
    private void Update()
    {
        RuleBook();
        if (_canWalk)
        {
            if (_timerInBetweenJumps > 0)
            {
                _timerInBetweenJumps -= Time.deltaTime;
            }

            if (Input.GetButtonDown("Jump"))
            {
                ///Sphere overlay over the player's feet.
                _groundCollisions = Physics.OverlapSphere(groundCheck.position, _groundCheckRadius, groundLayer);
                if (_groundCollisions.Length > 0)
                {
                    _canDoubleJump = true;
                    _timerInBetweenJumps = _refJumpTimer;
                    _rigidbody.AddForce((transform.up * _jumpheight * Time.deltaTime) * 100, ForceMode.Impulse);
                    _playerSounds.clip = _jumpSound;
                    _playerSounds.Play();
                    anim.SetBool("jump", true);
                }
                else {
                    if (_canDoubleJump && _activateDoubleJump && _timerInBetweenJumps <= 0)
                    {
                        _rigidbody.AddForce((transform.up * _jumpheight * Time.deltaTime) * 100, ForceMode.Impulse);
                        _canDoubleJump = false;
                        _playerSounds.clip = _jumpSound;
                        _playerSounds.Play();
                        anim.SetBool("jump", true);
                    }
                }
            }

            else {
                _rigidbody.angularVelocity = Vector3.zero;
                anim.SetBool("jump", false);

            }

            if (Input.GetButtonDown("Fire2") && _hasSword)
            {
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("sword attack"))
                {
                    anim.SetBool("attackSword", false);
                }
                else {
                    anim.SetBool("attackSword", true);
                }
            }
            else {
                anim.SetBool("attackSword", false);
            }

            if (Input.GetButtonDown("Fire3"))
            {
                SwitchBullets();
            }

            if (_canShoot == true)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("shooting"))
                    {
                        switch (_cProjectile)
                        {
                            case CurrentProjectile.Normal:
                                if (pVScript.Cannonballs > 0 && _hasCannon == true)
                                {
                                    AttackGun((int)CurrentProjectile.Normal);
                                    anim.SetBool("attackCannon", true);
                                    //ActivateSword(false, _hasSword);
                                }
                                else {
                                    _playerSounds.clip = _noAmmoSound;
                                    _playerSounds.Play();
                                    anim.SetBool("attackCannon", false);
                                }
                                break;
                            case CurrentProjectile.Explosive:
                                if (pVScript.ExplosiveCannonballs > 0 && _hasCannon == true)
                                {
                                    AttackGun((int)CurrentProjectile.Explosive);
                                    anim.SetBool("attackCannon", true);
                                    //ActivateSword(false, _hasSword);

                                }
                                else {
                                    _playerSounds.clip = _noAmmoSound;
                                    _playerSounds.Play();
                                    anim.SetBool("attackCannon", false);
                                }
                                break;
                        }
                        _canShoot = false;
                    }
                    else {
                        anim.SetBool("attackCannon", false);
                    }
                }
            }
            else if (_canShoot == false)
            {
                anim.SetBool("attackCannon", false);
                _shootTimer += Time.deltaTime;
                if (_shootTimer >= _shootCooldown)
                {
                    _shootTimer = 0;
                    _canShoot = true;
                }
            }

            //if (!anim.GetCurrentAnimatorStateInfo(0).IsName("shooting")) { }
            //{
            //    ActivateSword(_hasSword, _hasSword);
            //}
        }
    }
    private void FixedUpdate()
    {
        if (_canWalk)
        {
            float movement = Input.GetAxisRaw("Horizontal");
            _moveDirection = new Vector3(0, 0, movement);

            AnimateRunning(movement);

            if (_moveDirection.z > 0f)
            {
                SwitchLook(0f);
                _facesRight = true;
            }
            else if (_moveDirection.z < 0f)
            {
                SwitchLook(180f);
                _facesRight = false;
            }

            //After input > update the velocity here
            _moveDirection *= _speed;
            _rigidbody.velocity = new Vector3(_moveDirection.x, _rigidbody.velocity.y, _moveDirection.z);
        }
    }

    public void SetPlayerWalkable(bool pCanWalk)
    {
        _canWalk = pCanWalk;
    }

    private void SwitchBullets()
    {
        switch (_cProjectile)
        {
            case CurrentProjectile.Normal:
                _cProjectile = CurrentProjectile.Explosive;
                break;
            case CurrentProjectile.Explosive:
                _cProjectile = CurrentProjectile.Normal;
                break;
        }
        //TODO: Add Switch Sound
    }

    private void RuleBook()
    {
        //RuleBook is a collection of statements that should be in update.
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("sword attack"))
        {
            canDamage = true;
        }
        else {
            canDamage = false;
        }
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.CompareTag(Tags.SwordPickup))
        {
            c.gameObject.SetActive(false);
            ActivateSword(true, true);
            //TODO: Add feedback. (Huraay you've got a sword now!)
        }
        if (c.gameObject.CompareTag(Tags.CannonPickup))
        {
            c.gameObject.SetActive(false);
            ActivateCannon(true, true);
            //TODO: Add feedback. (Huraay you've got a cannon now!)
        }
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.CompareTag(Tags.Movable))
        {
            gameObject.transform.SetParent(c.transform, true);
        }
    }

    void OnCollisionExit(Collision c)
    {
        if (c.gameObject.CompareTag(Tags.Movable))
        {
            gameObject.transform.parent = null;
        }
    }
    public GameObject GetPlatform()
    {
        return currentPlatform;
    }

    private void AnimateRunning(float f)
    {
        bool Running = f != 0f;
        if (Running)
        {
            anim.SetBool("isRunning", true);
        }
        else {
            anim.SetBool("isRunning", false);
        }
    }

    public bool FacesRight { get { return _facesRight; } }
    public CurrentProjectile CurrentProj { get { return _cProjectile; } }
    public bool HasSword { get { return _hasSword; } }
    public bool HasCannon { get { return _hasCannon; } }
    public float ShootTimer { get { return _shootTimer; } }
    public float ShootCooldown { get { return _shootCooldown; } }
}