﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the player/enemy projectiles.
/// </summary>
public class ProjectileScript : MonoBehaviour
{
    private int _damage;
    public enum Owner
    {
        Player, Enemy
    }

    public enum Type
    {
        Normal, Explosive
    }
    private Owner _projOwner;
    private Type _projType;

    private void OnTriggerEnter(Collider c)
    {
        if (_projOwner == Owner.Player)
        {
            if (c.CompareTag(Tags.Enemy))
            {
                c.gameObject.GetComponent<Enemy>().Damage(_damage);
                Debug.Log(_projOwner + "'s projectile hit " + c.name);
            }
            else if (c.CompareTag(Tags.Player) || c.CompareTag(Tags.Sword))
            {
                return;
            }
        }
        else if (_projOwner == Owner.Enemy)
        {
            if (c.CompareTag(Tags.Player))
            {
                PlayerControls pControls = c.GetComponent<PlayerControls>();
                float projDistance = pControls.transform.position.z - this.transform.position.z;
                if (pControls.canDamage == true)
                {
                    //Does the player face the right way for parrying? So no parrying a bullet if the bullet hits your back
                    if ((projDistance < 0 && pControls.FacesRight == true) || (projDistance > 0 && pControls.FacesRight == false))
                    {
                        GetComponent<Rigidbody>().velocity *= -1;
                        _projOwner = Owner.Player;
                        Debug.Log(_projOwner + "'s projectile deflected");

                        pControls.outsideSounds.clip = pControls.swordDeflectSound;
                        pControls.outsideSounds.Play();
                        return;
                    }
                    else
                    {
                        c.gameObject.GetComponent<PlayerValues>().Damage(_damage);
                        Debug.Log(_projOwner + "'s projectile hit " + c.name);
                    } 
                }
                else
                {
                    c.gameObject.GetComponent<PlayerValues>().Damage(_damage);
                    Debug.Log(_projOwner + "'s projectile hit " + c.name);
                }
            }
            else if (c.CompareTag(Tags.Enemy) || c.CompareTag(Tags.Projectile))
            {
                return;
            }




        }

        Destroy(gameObject);
    }

    public int Damage { get { return _damage; } set { _damage = value; } }

    public Owner ProjOwner { get { return _projOwner; } set { _projOwner = value; } }
    public Type ProjType { get { return _projType; } set { _projType = value; } }

}