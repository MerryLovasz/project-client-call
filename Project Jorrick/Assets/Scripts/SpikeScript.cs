﻿using UnityEngine;
using System.Collections;

public class SpikeScript : MonoBehaviour {
    [SerializeField]
    private int _damage;
    [SerializeField]
    private float _coolDown = 2.0f;
    private float _coolDownTimer;
    private bool _canDamage = true;
    [SerializeField]
    private bool _usesTeleObj = true;
    [SerializeField]
    private GameObject _teleporter;
    private PlayerValues pVScript;

	void Update () {
        if (_canDamage == false)
        {
            _coolDownTimer += Time.deltaTime;

            if (_coolDownTimer >= _coolDown)
            {
                _coolDownTimer = 0;
                _canDamage = true;
            }
        }
    }

    void OnCollisionStay(Collision c)
    {
        if (c.collider.CompareTag(Tags.Player))
        {
            if (pVScript == null) pVScript = c.collider.GetComponent<PlayerValues>();
            if (_canDamage == true)
            {
                pVScript.Damage(_damage);
                _canDamage = false;
                if (_usesTeleObj == true && _damage < pVScript.Health)
                {
                    pVScript.transform.position = _teleporter.transform.position;
                }
            }
        }

    }
}
