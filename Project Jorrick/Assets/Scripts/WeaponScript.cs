﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour
{

    private Animator anim;
    private PlayerControls _player;
    private int _swordDamage;
    private bool _canDamage;

    private void Start()
    {
        _player = FindObjectOfType<PlayerControls>();
        anim = GetComponentInParent<Animator>();
        _swordDamage = _player.swordDamage;
    }

    private void OnTriggerEnter(Collider c)
    {
        _canDamage = _player.canDamage;
        if (c.CompareTag(Tags.Enemy))
        { 
            if (_canDamage)
            {
                _player.outsideSounds.clip = _player.swordHitSound;
                _player.outsideSounds.Play();
                c.gameObject.GetComponent<Enemy>().Damage(_swordDamage);
                Debug.Log("Player hit: Enemy");
            }
        }
        if (c.CompareTag(Tags.Destroyable))
        {
            if (_canDamage)
            {
                _player.outsideSounds.clip = _player.swordHitSound;
                _player.outsideSounds.Play();
                Debug.Log("Destroyed barrel");
                c.gameObject.GetComponent<BarrelScript>().DoDestroy();
            }
        }
    }
    public int SwordDamage { get { return _swordDamage; } }
}
