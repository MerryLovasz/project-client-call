﻿using UnityEngine;
using System.Collections.Generic;

public class WalkingEnemy : EnemyBase {

    private int currentWaypoint = 0;
    private List<Transform> waypoints;
    private int cooldown = 0;
    private state currentState;
    private Vector3 lastSeen;
    private int timesRotated = 0;
    private Renderer mRenderer;
    private bool playerIsRunning;
    private int oldHealth;
    private Animator anim;
    private NavMeshAgent agent;
    private ParticleSystem[] particles;

    public enum state
    {
        idle,
        attack,
        follow,
        investigate,
        rotate
    }

    public int Cooldown
    {
        get { return cooldown; }
        set { cooldown = value; }
    }

    public state CurrentState
    {
        get { return currentState; }
        set { currentState = value; }
    }

    public NavMeshAgent Agent
    {
        get { return agent; }
        set { agent = value; }
    }

    public List<Transform> Waypoints
    {
        get { return waypoints; }
    }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Awake------------------------------------------
    //------------------------------------------------------------------------------------------

    void Awake() {

        //gets all the components that are needed
        anim = GetComponent<Animator>();
        mRenderer = GetComponent<MeshRenderer>();
        agent = GetComponent<NavMeshAgent>();
        particles = DeathEffect.GetComponentsInChildren<ParticleSystem>();

        //setting the stats for this enemy
        Speed = DefaultSpeed;
        oldHealth = Health;
        RespawnTimeLeft = RespawnTimer;
        agent.speed = Speed;
        Health = BaseHealth;

        //finds the player and sets it as target
        TargetObject = GameObject.FindGameObjectWithTag(Tags.Player);
        Target = TargetObject.transform;

        //finds all the waypoints with this gameObject as owner and adds them to the list
        waypoints = new List<Transform>();
        foreach (GameObject waypointObject in GameObject.FindGameObjectsWithTag(Tags.Waypoint))
        {
            if (waypointObject.GetComponent<WaypointScript>().Owner == this)
            waypoints.Add(waypointObject.transform);
        }
        if (waypoints.Count == 0)
        {
            Debug.LogWarning("Warning: No waypoints were found for " + this.gameObject);
        }
        else agent.destination = waypoints[0].position;

        currentState= state.idle;
	}

    //------------------------------------------------------------------------------------------
    //--------------------------------------Update-----------------------------------------
    //------------------------------------------------------------------------------------------

    void Update()
    {
        //checking what state the player is in
        if (currentState == state.idle)
        {
            anim.SetInteger("state", 1);
            Speed = DefaultSpeed;
            mRenderer.material.color = Color.white;
            idle();
        }else if (currentState == state.follow)
        {
            anim.SetInteger("state", 2);
            Speed = ChaseSpeed;
            mRenderer.material.color = Color.red;
            follow();
        }else if (currentState == state.attack)
        {
            mRenderer.material.color = Color.blue;
            attack();
        }else if (currentState == state.investigate)
        {
            anim.SetInteger("state", 3);
            Speed = DefaultSpeed;
            mRenderer.material.color = Color.yellow;
            investigate();
        } else if (currentState == state.rotate)
        {
            anim.SetInteger("state", 4);
            Speed = DefaultSpeed;
            agent.destination = transform.position;
            mRenderer.material.color = Color.green;
            if (doRaycast() && Vector3.Distance(transform.position, Target.position) < TargetRange && Vector3.Distance(transform.position, Target.position) > TargetAttackRange) currentState = state.follow;
            else if (rotated())
            {
                currentState = state.idle;
            }
        }

        doRaycast();

        agent.speed = Speed;
        checkDeath();
        cooldown--;
        oldHealth = Health;
    }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Collisions--------------------------------------
    //------------------------------------------------------------------------------------------

    //if the player walks into the enemy
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag(Tags.Player))
        {
            currentState = state.follow;
        }
    }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Raycasting------------------------------------
    //------------------------------------------------------------------------------------------

    private bool doRaycast()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, (Target.position-transform.position).normalized, out hit))
        {
            if (Vector3.Angle(transform.forward, hit.transform.position- transform.position) < Fov)
            {
                if (hit.collider.gameObject.CompareTag(Tags.Player))
                {
                    lastSeen = new Vector3(Target.position.x, Target.position.y, Target.position.z);
                    return true;
                }
            }
        }
        return false;
    }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Attacking--------------------------------------
    //------------------------------------------------------------------------------------------

    public virtual void attack()
    {
        //attacks if the cooldown is 0
        Player player = Target.GetComponent<Player>();
		if (player != null) {
			if (cooldown <= 0) {
				player.Health -= Damage;
				cooldown = AttackCooldown;
                currentState = state.follow;
                agent.speed = ChaseSpeed;
			}
		}
    }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Idling--------------------------------------------
    //------------------------------------------------------------------------------------------

    private void idle()
    {
        Player player = (Player)TargetObject.GetComponent<Player>();
        playerIsRunning = player.IsRunning;

        if (doRaycast() && Vector3.Distance(transform.position, Target.position) < TargetRange && Vector3.Distance(transform.position, Target.position) > TargetAttackRange) currentState = state.follow;
        else if (tookDamage()) currentState = state.rotate;
        else if (playerIsRunning && Vector3.Distance(Target.position, transform.position) < 10)
        {
            lastSeen = new Vector3(Target.position.x, Target.position.y, Target.position.z);
            currentState = state.investigate;
            return;
        }
        else if (doRaycast() && Vector3.Distance(transform.position, Target.position) < TargetAttackRange) currentState = state.follow;
        

        //makes the player move between all the set waypoints
        if (waypoints.Count > 0)
        {
            agent.destination = waypoints[currentWaypoint].position;

        if (Vector3.Distance(transform.position, waypoints[currentWaypoint].transform.position) <= 1f) getNextWaypoint();
        }
    }

    private void getNextWaypoint()
    {
        //gets the next waypoint in the list to move towards
        currentWaypoint++;
        if (currentWaypoint > waypoints.Count-1) currentWaypoint = 0;
        agent.destination = waypoints[currentWaypoint].position;
    }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Following--------------------------------------
    //------------------------------------------------------------------------------------------

    //move towards the player
    private void follow()
    {
        agent.destination = Target.position;
        if (!doRaycast() || Vector3.Distance(Target.transform.position, transform.position) > TargetRange) currentState = state.investigate;
        if (Vector3.Distance(Target.transform.position, transform.position) < TargetAttackRange) {
            currentState = state.attack;
            anim.SetInteger("state", 5);
            }
        }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Investigating----------------------------------
    //------------------------------------------------------------------------------------------

    //move to where the player was last seen
    private void investigate()
    {
        if (doRaycast() && Vector3.Distance(transform.position, Target.position) < TargetRange && Vector3.Distance(transform.position, Target.position) > TargetAttackRange) currentState = state.follow;
        else if (tookDamage()) currentState = state.rotate;
        agent.destination = lastSeen;
        if (agent.remainingDistance <= 0.1f) currentState = state.rotate;
     }

    //------------------------------------------------------------------------------------------
    //--------------------------------------Rotating----------------------------------------
    //------------------------------------------------------------------------------------------

    //return true if the enemy has rotated 360 degree
    private bool rotated()
    {
        if (timesRotated < 180)
        {
            transform.Rotate(Vector3.up, 2);
            timesRotated++;
            return false;
        }
        timesRotated = 0;
        return true;
    }
    
    //------------------------------------------------------------------------------------------
    //--------------------------------------Dying-------------------------------------------
    //------------------------------------------------------------------------------------------

    //if the player took damage in the last frame this will return true
    private bool tookDamage()
    {
        if (Health != oldHealth) return true;
        return false;
    }

    private void checkDeath()
    {
        //checks if the enemy is dead and if so deletes the gameObject
        if (Health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        //play the death effect
        DeathEffect.transform.position = transform.position;

        foreach (ParticleSystem particle in particles)
        {
            ParticleSystem DeathParticle = Instantiate(particle) as ParticleSystem;
            DeathParticle.transform.position = transform.position;
            DeathParticle.loop = false;
            DeathParticle.Play();
        }

        //delete the player until it respawns
        Died = true;
        gameObject.SetActive(false);
    }
}
